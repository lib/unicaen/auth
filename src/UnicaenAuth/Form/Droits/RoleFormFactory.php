<?php

namespace UnicaenAuth\Form\Droits;

use Psr\Container\ContainerInterface;
use UnicaenAuth\Service\RoleService;

class RoleFormFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $form = new RoleForm();

        /** @var RoleService $serviceRole */
        $serviceRole = $container->get('UnicaenAuth\Service\Role');

        $form->setServiceRole($serviceRole);

        return $form;
    }
}
<?php

namespace UnicaenAuth\Form;

use ZfcUser\Options\AuthenticationOptionsInterface;

class CasLoginForm extends LoginForm
{
    /**
     * @var string[]
     */
    protected $types = ['cas'];

    /**
     * CasLoginForm constructor.
     * @param $name
     * @param AuthenticationOptionsInterface $options
     */
    public function __construct($name, AuthenticationOptionsInterface $options)
    {
        parent::__construct($name, $options);

        $this->remove('identity');
        $this->remove('credential');

        $this->get('submit')->setLabel("Authentification centralisée");
    }
}

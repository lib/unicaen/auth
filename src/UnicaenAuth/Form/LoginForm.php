<?php

namespace UnicaenAuth\Form;

use Laminas\Http\Request;
use ZfcUser\Form\Login;

class LoginForm extends Login
{
    /**
     * @var string[]
     */
    protected $types = ['db', 'ldap'];

    /**
     * @var bool
     */
    protected $hidden = false;

    /**
     * @param string[] $types
     * @return self
     */
    public function setTypes(array $types): self
    {
        $this->types = $types;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getTypes(): array
    {
        return $this->types;
    }

    /**
     * @param \Laminas\Http\Request $request
     * @return void
     */
    public function initFromRequest(Request $request)
    {

    }

    /**
     * @return bool
     */
    public function isHidden(): bool
    {
        return $this->hidden;
    }

    /**
     * @param bool $hidden
     * @return self
     */
    public function setHidden(bool $hidden): self
    {
        $this->hidden = $hidden;
        return $this;
    }
}

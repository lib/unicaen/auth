<?php

namespace UnicaenAuth\Authentication\Storage;

use UnicaenAuth\Authentication\SessionIdentity;
use UnicaenAuth\Options\Traits\ModuleOptionsAwareTrait;
use Laminas\Authentication\Storage\StorageInterface;

abstract class AbstractStorage implements ChainableStorage
{
    use ModuleOptionsAwareTrait;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var StorageInterface
     */
    protected $storage;

    /**
     * @var mixed
     */
    protected $resolvedIdentity;

    /**
     * @param StorageInterface $storage
     * @return self
     */
    public function setStorage(StorageInterface $storage): self
    {
        $this->storage = $storage;
        return $this;
    }

    /**
     * @param ChainEvent $e
     */
    public function write(ChainEvent $e)
    {
        $contents = $e->getParam('contents');

        $this->resolvedIdentity = null;
        $this->storage->write($contents);
    }

    /**
     * @inheritDoc
     */
    public function read(ChainEvent $e)
    {
        /** @var SessionIdentity $sessionIdentity */
        $sessionIdentity = $this->storage->read();
        if ($sessionIdentity === null) {
            return null;
        }

        if ($this->resolvedIdentity === null) {
            if ($this->canHandleContents($sessionIdentity)) {
                $this->resolvedIdentity = $this->findIdentity();
            }
        }

        $e->addContents($this->type, $this->resolvedIdentity);
    }

    /**
     * @param ChainEvent $e
     */
    public function clear(ChainEvent $e)
    {
        $this->resolvedIdentity = null;
        $this->storage->clear();
    }
    /**
     * Recherche l'entité correspondant aux données d'authentification présentes en session.
     *
     * @return mixed
     */
    abstract protected function findIdentity();

    /**
     * Détermine si ce storage sait gérer le type de données de session spécifiées.
     *
     * @param SessionIdentity $sessionIdentity
     * @return bool
     */
    protected function canHandleContents(SessionIdentity $sessionIdentity): bool
    {
        return $sessionIdentity->typeIs($this->type);
    }
}
<?php

namespace UnicaenAuth\Authentication\Storage;

use Psr\Container\ContainerInterface;
use Laminas\Authentication\Storage\Session;
use Laminas\Session\Exception\RuntimeException;
use Laminas\Session\SessionManager;

class AuthFactory
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @return Auth
     */
    public function __invoke(ContainerInterface $container, string $requestedName): Auth
    {
        /** @var SessionManager $sessionManager */
        $sessionManager = $container->get(SessionManager::class);

        $storage = new Auth();

        try {
            $storage->setStorage(new Session(Usurpation::class, null, $sessionManager));
        } catch (RuntimeException $e) {
            /**
             * Tentative de réagir en cas d'erreur suivante :
             * "PHP Fatal error:  Uncaught Zend\Session\Exception\RuntimeException: Session validation failed in
             * /var/www/sygal/vendor/zendframework/zend-session/src/SessionManager.php:160"
             */
            $sessionManager->regenerateId(true);
            $storage->setStorage(new Session(Usurpation::class, null, $sessionManager));
        }

        return $storage;
    }
}
<?php

namespace UnicaenAuth\Authentication\Storage;

use Psr\Container\ContainerInterface;
use UnicaenAuth\Options\ModuleOptions;
use UnicaenAuth\Service\ShibService;
use Laminas\Authentication\Storage\Session;
use Laminas\Session\SessionManager;

class ShibFactory
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $moduleOptions
     * @return Shib
     */
    public function __invoke(ContainerInterface $container, string $requestedName, array $moduleOptions = null)
    {
        /** @var ShibService $shibService */
        $shibService = $container->get(ShibService::class);

        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');

        /** @var SessionManager $sessionManager */
        $sessionManager = $container->get(SessionManager::class);

        $storage = new Shib();
        $storage->setStorage(new Session(\UnicaenAuth\Authentication\Adapter\Shib::class, null, $sessionManager));
        $storage->setShibService($shibService);
        $storage->setModuleOptions($moduleOptions);

        return $storage;
    }
}
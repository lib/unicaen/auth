<?php

namespace UnicaenAuth\Authentication\Storage;

use Laminas\Authentication\Exception\ExceptionInterface;
use Laminas\Authentication\Storage\Session;
use Laminas\Authentication\Storage\StorageInterface;
use Laminas\EventManager\EventManagerAwareInterface;
use Laminas\EventManager\EventManagerInterface;

/**
 * Implémentation d'une chaîne de responsabilité permettant à plusieurs sources
 * de fournir les données sur l'identité authentifiée éventuelle.
 *
 * Exemples de sources disponibles :
 *  - Ldap (annuaire LDAP)
 *  - Db (table des utilisateurs en base de données)
 *  - Shib (fédération d'identité shibboleth)
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 * @see ChainEvent
 * @see \UnicaenAuth\Service\ChainAuthenticationStorageServiceFactory
 * @see Ldap
 * @see Db
 * @see Shib
 */
class Chain implements StorageInterface, EventManagerAwareInterface
{
    /**
     * @var StorageInterface
     */
    protected $storage;

    /**
     * @var EventManagerInterface
     */
    protected $eventManager;

    /**
     * @var ChainEvent
     */
    protected $event;

    /**
     * @var array
     */
    protected $resolvedIdentity;

    /**
     * @inheritDoc
     */
    public function isEmpty(): bool
    {
        return $this->getStorage()->isEmpty();
    }

    /**
     * @inheritDoc
     */
    public function read(): array
    {
        if (null !== $this->resolvedIdentity) {
            return $this->resolvedIdentity;
        }

        // interrogation de tous les storages écoutant l'événement 'read'
        $e = $this->getEvent();
        $e->setName('read');
        $this->getEventManager()->triggerEvent($e);

        $this->resolvedIdentity = $e->getContents();

        return $this->resolvedIdentity;
    }

    /**
     * Writes $contents to storage
     *
     * @param  mixed $contents
     * @throws ExceptionInterface If writing $contents to storage is impossible
     * @return void
     */
    public function write($contents)
    {
        $this->getStorage()->write($contents);

        $e = $this->getEvent();
        $e->setName('write');
        $e->setParams(compact('contents'));
        $this->getEventManager()->triggerEvent($e);
    }

    /**
     * Clears contents from storage
     *
     * @throws ExceptionInterface If clearing contents from storage is impossible
     * @return void
     */
    public function clear()
    {
        $this->getStorage()->clear();

        $e = $this->getEvent();
        $e->setName('clear');
        $this->getEventManager()->triggerEvent($e);
    }

    /**
     * getStorage
     *
     * @return StorageInterface
     */
    public function getStorage(): StorageInterface
    {
        if (null === $this->storage) {
            $this->setStorage(new Session());
        }
        return $this->storage;
    }

    /**
     * setStorage
     *
     * @param StorageInterface $storage
     * @return self
     */
    public function setStorage(StorageInterface $storage): self
    {
        $this->storage = $storage;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setEventManager(EventManagerInterface $eventManager): self
    {
        $eventManager->setIdentifiers([
            __CLASS__,
            get_called_class(),
        ]);
        $this->eventManager = $eventManager;
        return $this;
    }

    /**
     * Retrieve the event manager
     *
     * Lazy-loads an EventManager instance if none registered.
     *
     * @return EventManagerInterface
     */
    public function getEventManager(): EventManagerInterface
    {
        return $this->eventManager;
    }

    /**
     * @return ChainEvent
     */
    public function getEvent(): ChainEvent
    {
        if (null === $this->event) {
            $this->event = new ChainEvent();
        }
        return $this->event;
    }

    /**
     * @param ChainEvent $event
     * @return self
     */
    public function setEvent(ChainEvent $event): self
    {
        $this->event = $event;
        return $this;
    }
}
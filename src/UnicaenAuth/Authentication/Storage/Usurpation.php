<?php

namespace UnicaenAuth\Authentication\Storage;

use UnicaenAuth\Authentication\SessionIdentity;
use UnicaenAuth\Entity\Db\AbstractUser;
use ZfcUser\Mapper\UserInterface as UserMapper;

/**
 * Storage chargé de stocker/restituer des infos concernant l'usurpation en cours éventuelle.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class Usurpation extends AbstractStorage
{
    const TYPE = 'usurpation';
    const KEY_USURPATEUR = 'usurpateur';

    /**
     * @var string
     */
    protected $type = self::TYPE;

    /**
     * @var UserMapper
     */
    protected $mapper;

    /**
     * @param array $identityArray
     * @return AbstractUser|null
     */
    public static function extractUsurpateurFromIdentityArray(array $identityArray): ?AbstractUser
    {
        return $identityArray[self::TYPE][self::KEY_USURPATEUR] ?? null;
    }

    /**
     * @inheritDoc
     */
    protected function canHandleContents(SessionIdentity $sessionIdentity): bool
    {
        return $sessionIdentity->isUsurpation();
    }

    /**
     * setMapper
     *
     * @param UserMapper|null $mapper
     * @return self
     */
    public function setMapper(UserMapper $mapper = null): self
    {
        $this->mapper = $mapper;
        return $this;
    }

    /**
     * @inheritDoc
     */
    protected function findIdentity(): array
    {
        /** @var SessionIdentity $sessionIdentity */
        $sessionIdentity = $this->storage->read();
        $usernameUsurpateur = $sessionIdentity->getUsurpateur();

        return [
            self::KEY_USURPATEUR => $this->mapper->findByUsername($usernameUsurpateur),
        ];
    }
}
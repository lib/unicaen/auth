<?php

namespace UnicaenAuth\Authentication\Storage;

use UnicaenApp\Mapper\Ldap\People as LdapPeopleMapper;
use UnicaenAuth\Authentication\Adapter\Cas;
use UnicaenAuth\Authentication\SessionIdentity;
use UnicaenAuth\Entity\Ldap\People;

/**
 * Ldap authentication storage.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class Ldap extends AbstractStorage
{
    const TYPE = \UnicaenAuth\Authentication\Adapter\Ldap::TYPE;

    /**
     * @var string
     */
    protected $type = self::TYPE;

    /**
     * @var LdapPeopleMapper
     */
    protected $mapper;

    /**
     * @inheritDoc
     */
    protected function canHandleContents(SessionIdentity $sessionIdentity): bool
    {
        return parent::canHandleContents($sessionIdentity) || $sessionIdentity->typeIs(Cas::TYPE);
    }

    /**
     * @return bool
     */
    protected function isEnabled(): bool
    {
        $configLdap = $this->moduleOptions->getLdap();
        $configCas = $this->moduleOptions->getCas();

        return
            isset($configLdap['enabled']) && (bool) $configLdap['enabled'] ||
            isset($configCas['enabled']) && (bool) $configCas['enabled'];
    }

    /**
     * Recherche l'entité LDAP correspondant aux données présentes en session.
     *
     * @return People|null
     */
    protected function findIdentity(): ?People
    {
        /** @var SessionIdentity $sessionIdentity */
        $sessionIdentity = $this->storage->read();
        $username = $sessionIdentity->getUsername();
        $identity = $this->getMapper()->findOneByUsername($username);

        if ($identity === null) {
            return null;
        }

        return new People($identity);
    }

    /**
     * getMapper
     *
     * @return LdapPeopleMapper
     */
    public function getMapper(): LdapPeopleMapper
    {
        return $this->mapper;
    }

    /**
     * setMapper
     *
     * @param LdapPeopleMapper|null $mapper
     * @return self
     */
    public function setMapper(LdapPeopleMapper $mapper = null): self
    {
        $this->mapper = $mapper;
        return $this;
    }
}

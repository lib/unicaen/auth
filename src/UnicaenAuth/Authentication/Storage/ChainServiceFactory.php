<?php

namespace UnicaenAuth\Authentication\Storage;

use Psr\Container\ContainerInterface;
use UnicaenAuth\Event\EventManager;
use UnicaenAuth\Options\ModuleOptions;

/**
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class ChainServiceFactory
{
    private $mandatoryStorages = [
        900 => Auth::class,
        200 => 'UnicaenAuth\Authentication\Storage\Ldap',
        100 => 'UnicaenAuth\Authentication\Storage\Db',
        75  => 'UnicaenAuth\Authentication\Storage\Shib',
        10  => Usurpation::class, // en dernier
    ];

    protected $storages = [];

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return Chain
     */
    public function __invoke(ContainerInterface $container, string $requestedName, array $options = null): Chain
    {
        $chain = new Chain();

        /** @var EventManager $eventManager */
        $eventManager = $container->get(EventManager::class);

        $chain->setEventManager($eventManager);

        /** @var ModuleOptions $options */
        $options = $container->get('unicaen-auth_module_options');

        // retrait du fournisseur Ldap si l'auth LDAP est désactivée
        if (isset($options->getLdap()['enabled']) && ! $options->getLdap()['enabled']) {
            unset($this->mandatoryStorages[200]);
        }

        $storages = $this->mandatoryStorages + $this->storages;
        krsort($storages);

        foreach ($storages as $priority => $name) {
            $storage = $container->get($name);
            $chain->getEventManager()->attach('read', [$storage, 'read'], $priority);
            $chain->getEventManager()->attach('write', [$storage, 'write'], $priority);
            $chain->getEventManager()->attach('clear', [$storage, 'clear'], $priority);
        }

        return $chain;
    }
}
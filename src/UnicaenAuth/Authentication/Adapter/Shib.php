<?php

namespace UnicaenAuth\Authentication\Adapter;

use Laminas\Authentication\Result as AuthenticationResult;
use Laminas\EventManager\EventInterface;
use Laminas\Http\Response;
use Laminas\Router\RouteInterface;
use UnicaenAuth\Options\Traits\ModuleOptionsAwareTrait;
use UnicaenAuth\Service\Traits\ShibServiceAwareTrait;
use UnicaenAuth\Service\User;
use ZfcUser\Authentication\Adapter\AdapterChainEvent;

/**
 * CAS authentication adpater
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class Shib extends AbstractAdapter
{
    use ModuleOptionsAwareTrait;
    use ShibServiceAwareTrait;

    const TYPE = 'shib';

    /**
     * @var string
     */
    protected $type = self::TYPE;

    /**
     * @var RouteInterface
     */
    private $router;

    /**
     * @param RouteInterface $router
     */
    public function setRouter(RouteInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @var User
     */
    private $userService;

    /**
     * @param User $userService
     */
    public function setUserService(User $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @inheritDoc
     */
    public function authenticate(EventInterface $e)
    {
        // NB: Dans la version 3.0.0 de zf-commons/zfc-user, cette méthode prend un EventInterface.
        // Mais dans la branche 3.x, c'est un AdapterChainEvent !
        // Si un jour c'est un AdapterChainEvent qui est attendu, plus besoin de faire $e->getTarget().
        $event = $e->getTarget(); /** @var AdapterChainEvent $event */

        $type = $event->getRequest()->getPost()->get('type');
        if ($type !== $this->type) {
            return false;
        }

        $shibUser = $this->shibService->getAuthenticatedUser();

        if ($shibUser === null) {
            $redirectUrl = $this->router->assemble(['type' => 'shib'], [
                    'name' => 'zfcuser/authenticate',
                    'query' => ['redirect' => $event->getRequest()->getQuery()->get('redirect')]]
            );
            $shibbolethTriggerUrl = $this->router->assemble([], [
                    'name' => 'auth/shibboleth',
                    'query' => ['redirect' => $redirectUrl]]
            );
            $response = new Response();
            $response->getHeaders()->addHeaderLine('Location', $shibbolethTriggerUrl);
            $response->setStatusCode(302);

            return $response;
        }

        $identity = $this->createSessionIdentity($shibUser->getEppn());

        $event->setIdentity($identity);
        $this->setSatisfied(true);
        $storage = $this->getStorage()->read();
        $storage['identity'] = $event->getIdentity();
        $this->getStorage()->write($storage);
        $event
            ->setCode(AuthenticationResult::SUCCESS)
            ->setMessages(['Authentication successful.']);

        /* @var $userService User */
        $this->userService->userAuthenticated($shibUser);

        return true;
    }

    public function logout(EventInterface $e)
    {
        parent::logout($e);

        $storage = $this->getStorage()->read();
        if (! isset($storage['identity'])) {
            return;
        }

        // désactivation de l'usurpation d'identité éventuelle
        $this->shibService->deactivateUsurpation();

        // URL vers laquelle on redirige après déconnexion
        $returnUrl = $this->router->assemble([], [
                'name' => 'zfcuser/logout',
                'force_canonical' => true,
        ]);
        $shibbolethLogoutUrl = $this->shibService->getLogoutUrl($returnUrl);

        $response = new Response();
        $response->getHeaders()->addHeaderLine('Location', $shibbolethLogoutUrl);
        $response->setStatusCode(302);
    }
}
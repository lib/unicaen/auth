<?php

namespace UnicaenAuth\Authentication\Adapter;

use Psr\Container\ContainerInterface;
use UnicaenAuth\Options\ModuleOptions;
use Laminas\Authentication\Storage\Session;
use Laminas\EventManager\EventManager;

class LocalAdapterFactory
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @return LocalAdapter
     */
    public function __invoke(ContainerInterface $container, string $requestedName)
    {
        /** @var ModuleOptions $options */
        $options = $container->get('unicaen-auth_module_options');

        $localAdapter = new LocalAdapter();
        $localAdapter->setEventManager(new EventManager());
        $localAdapter->setStorage(new Session(LocalAdapter::class));
        $localAdapter->setModuleOptions($options);

        $localConfig = $options->getLocal();

        // db adapter
        if (array_key_exists('db', $localConfig)) {
            $this->attachSubAdapter($localAdapter, $container, $localConfig['db'], 20); // en 1er
        }
        // ldap adapter
        if (array_key_exists('ldap', $localConfig)) {
            $this->attachSubAdapter($localAdapter, $container, $localConfig['ldap'], 10); // en 2e
        }

        return $localAdapter;
    }

    /**
     * @param LocalAdapter $localAdapter
     * @param ContainerInterface $container
     * @param array $config
     * @param int $priority
     */
    private function attachSubAdapter(LocalAdapter $localAdapter, ContainerInterface $container, array $config, int $priority)
    {
        $enabled = isset($config['enabled']) && $config['enabled'] === true;
        if (! $enabled) {
            return;
        }

        /** @var AbstractAdapter $subAdapter */
        $subAdapter = $container->get($config['adapter']);
        $subAdapter->attach($localAdapter->getEventManager(), $priority);
    }

}
<?php

namespace UnicaenAuth\Authentication\Adapter;

use Psr\Container\ContainerInterface;
use UnicaenApp\Mapper\Ldap\People as LdapPeopleMapper;
use UnicaenAuth\Options\ModuleOptions;
use UnicaenAuth\Service\User;
use Laminas\Authentication\Storage\Session;

class LdapAdapterFactory
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return Ldap
     */
    public function __invoke(ContainerInterface $container, string $requestedName, array $options = null)
    {
        $adapter = new Ldap();
        $adapter->setStorage(new Session(Ldap::class));

        $this->injectDependencies($adapter, $container);

//        /** @var EventManager $eventManager */
//        $eventManager = $container->get(EventManager::class);
//        $adapter->setEventManager($eventManager);
//        $userService = $container->get('unicaen-auth_user_service'); /* @var $userService \UnicaenAuth\Service\User */
//        $eventManager->attach('userAuthenticated', [$userService, 'userAuthenticated'], 100);
//        $eventManager->attach('clear', function() use ($adapter){
//            $adapter->getStorage()->clear();
//        });

        return $adapter;
    }

    /**
     * @param Ldap $adapter
     * @param ContainerInterface $container
     */
    private function injectDependencies(Ldap $adapter, ContainerInterface $container)
    {
        /** @var User $userService */
        $userService = $container->get('unicaen-auth_user_service');
        $adapter->setUserService($userService);

        /** @var LdapPeopleMapper $ldapPeopleMapper */
        $ldapPeopleMapper = $container->get('ldap_people_mapper');
        $adapter->setLdapPeopleMapper($ldapPeopleMapper);

        $options = array_merge(
            $container->get('zfcuser_module_options')->toArray(),
            $container->get('unicaen-auth_module_options')->toArray());
        $moduleOptions = new ModuleOptions($options);
        $adapter->setModuleOptions($moduleOptions);

        $substitut = $moduleOptions->getLdap()['type'] ?? null;
        if ($substitut !== null) {
            $adapter->setType($substitut);
        }

        /** @var \UnicaenApp\Options\ModuleOptions $appModuleOptions */
        $appModuleOptions = $container->get('unicaen-app_module_options');
        $adapter->setAppModuleOptions($appModuleOptions);
    }
}
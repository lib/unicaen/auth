<?php

namespace UnicaenAuth\Authentication\Adapter;

use Laminas\Authentication\Storage\Session;
use Laminas\Router\RouteInterface;
use Psr\Container\ContainerInterface;
use UnicaenAuth\Options\ModuleOptions;
use UnicaenAuth\Service\ShibService;
use UnicaenAuth\Service\User;

class ShibAdapterFactory
{
    public function __invoke(ContainerInterface $container, string $requestedName, array $options = null)
    {
        $adapter = new Shib();
        $adapter->setStorage(new Session(Shib::class));

        $this->injectDependencies($adapter, $container);

        return $adapter;
    }

    /**
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     */
    private function injectDependencies(Shib $adapter, ContainerInterface $container)
    {
        /** @var ShibService $shibService */
        $shibService = $container->get(ShibService::class);
        $adapter->setShibService($shibService);

        /** @var RouteInterface $router */
        $router = $container->get('router');
        $adapter->setRouter($router);

        /** @var User $userService */
        $userService = $container->get('unicaen-auth_user_service');
        $adapter->setUserService($userService);

        $options = array_merge(
            $container->get('zfcuser_module_options')->toArray(),
            $container->get('unicaen-auth_module_options')->toArray());
        $moduleOptions = new ModuleOptions($options);
        $adapter->setModuleOptions($moduleOptions);

        // type alias
        $substitut = $moduleOptions->getShib()['type'] ?? null;
        if ($substitut !== null) {
            $adapter->setType($substitut);
        }
    }
}
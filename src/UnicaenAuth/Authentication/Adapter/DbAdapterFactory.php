<?php

namespace UnicaenAuth\Authentication\Adapter;

use Psr\Container\ContainerInterface;
use UnicaenAuth\Options\ModuleOptions;
use Laminas\Authentication\Storage\Session;
use ZfcUser\Mapper\UserInterface as UserMapperInterface;

class DbAdapterFactory
{
    /**
     * @param ContainerInterface $container
     * @return Db
     */
    public function __invoke(ContainerInterface $container): Db
    {
        /** @var UserMapperInterface $userMapper */
        $userMapper = $container->get('zfcuser_user_mapper');

        $adapter = new Db();
        $adapter->setStorage(new Session(Db::class));
        $adapter->setMapper($userMapper);

        $options = array_merge(
            $container->get('zfcuser_module_options')->toArray(),
            $container->get('unicaen-auth_module_options')->toArray());
        $moduleOptions = new ModuleOptions($options);
        $adapter->setModuleOptions($moduleOptions);

        $substitut = $moduleOptions->getDb()['type'] ?? null;
        if ($substitut !== null) {
            $adapter->setType($substitut);
        }

        return $adapter;
    }
}
<?php

namespace UnicaenAuth\ORM\Event\Listeners;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;
use Psr\Container\ContainerInterface;

class HistoriqueListenerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    /**
     * **ATTENTION : Précédemment, on injectait {@see \Laminas\Authentication\AuthenticationService}
     * mais cela créait une boucle infinie de dépendances de services.
     * Donc on a choisi d'injecter le service manager :-( et c'est HistoriqueListener qui récupère
     * lui-même le {@see \Laminas\Authentication\AuthenticationService} au moment opportun.**
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new HistoriqueListener($container);
    }
}

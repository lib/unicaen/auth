<?php

namespace UnicaenAuth\Event\Listener;

use UnicaenApp\Service\EntityManagerAwareInterface;
use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenAuth\Entity\Db\AbstractUser;
use UnicaenAuth\Event\UserAuthenticatedEvent;
use UnicaenAuth\Service\Traits\UserContextServiceAwareTrait;
use Laminas\EventManager\EventManagerInterface;
use Laminas\EventManager\ListenerAggregateInterface;
use Laminas\EventManager\ListenerAggregateTrait;

/**
 * Classe abstraites pour les classes désirant scruter un événement déclenché lors de l'authentification
 * utilisateur.
 *
 * Événements disponibles :
 * - juste avant que l'entité utilisateur ne soit persistée.
 * - juste après que l'entité utilisateur ait été persistée.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 * @see UserAuthenticatedEvent
 */
abstract class AuthenticatedUserSavedAbstractListener implements ListenerAggregateInterface, EntityManagerAwareInterface
{
    use ListenerAggregateTrait;
    use EntityManagerAwareTrait;
    use UserContextServiceAwareTrait;

    /**
     * @var callable[]
     */
    protected $listeners = [];

    /**
     * Méthode appelée juste avant que l'entité utilisateur soit persistée.
     *
     * @param UserAuthenticatedEvent $e
     * @return void
     */
    public function onUserAuthenticatedPrePersist(UserAuthenticatedEvent $e)
    {
        /** @var AbstractUser $user */
        $user = $e->getDbUser();

        // Sélection du dernier rôle endossé.
        $this->selectLastUserRole($user);
    }

    /**
     * Méthode appelée juste après que l'entité utilisateur soit persistée.
     *
     * @param UserAuthenticatedEvent $e
     * @return void
     */
    public function onUserAuthenticatedPostPersist(UserAuthenticatedEvent $e)
    {
        // nop
    }

    protected function selectLastUserRole(AbstractUser $user)
    {
        if ($role = $user->getLastRole()) {
            $this->serviceUserContext->setNextSelectedIdentityRole($role);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $sharedEvents = $events->getSharedManager();

        $this->listeners[] = $sharedEvents->attach(
            'UnicaenAuth\Service\User',
            UserAuthenticatedEvent::PRE_PERSIST,
            [$this, 'onUserAuthenticatedPrePersist'],
            100);

        $this->listeners[] = $sharedEvents->attach(
            'UnicaenAuth\Service\User',
            UserAuthenticatedEvent::POST_PERSIST,
            [$this, 'onUserAuthenticatedPostPersist'],
            100);
    }
}
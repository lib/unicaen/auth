<?php

namespace UnicaenAuth\Event\Listener;

use Psr\Container\ContainerInterface;

class LdapAuthenticationFailureLoggerListenerFactory
{
    public function __invoke(ContainerInterface $container): LdapAuthenticationFailureLoggerListener
    {
        return new LdapAuthenticationFailureLoggerListener();
    }
}
<?php

namespace UnicaenAuth\Event\Listener;

use Psr\Log\LoggerInterface;
use UnicaenAuth\Authentication\Adapter\Ldap;
use Laminas\Authentication\Result;
use Laminas\EventManager\Event;
use Laminas\EventManager\EventManagerInterface;
use Laminas\EventManager\ListenerAggregateInterface;
use Laminas\EventManager\ListenerAggregateTrait;

/**
 * Permet de scruter les erreurs d'authentification LDAP pour les ajouter aux logs.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class LdapAuthenticationFailureLoggerListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    public function setLogger(LoggerInterface $logger): self
    {
        $this->logger = $logger;
        return $this;
    }

    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->getSharedManager()->attach(
            "*",
            Ldap::LDAP_AUTHENTIFICATION_FAIL,
            [$this, "onLdapError"],
            100
        );
    }

    public function onLdapError(Event $event)
    {
        /** @var \Laminas\Authentication\Result $result */
        $result = $event->getParam('result');
        $details = $result->getMessages() ?: ["Aucun détail supplémentaire."];
        $username = $event->getParam('username');

        $messages = [];
        switch ($result->getCode()) {
            case Result::FAILURE_IDENTITY_NOT_FOUND:
                $messages[] = "Identifiant de connexion inconnu : '$username'.";
                break;
            case Result::FAILURE_CREDENTIAL_INVALID:
                $messages[] = "Mot de passe incorrect pour l'identifiant de connexion '$username'.";
                break;
            case Result::FAILURE:
            default:
                $messages[] = "Erreur rencontrée lors de l'authentification : ";
                break;
        }
        $messages = array_merge($messages, $details);
        $error = "[SyGAL LDAP AUTH] " . implode(PHP_EOL, $messages);

        $this->log($error);
    }

    protected function log(string $message)
    {
        if ($this->logger !== null) {
            $this->logger->error($message);
        } else {
            error_log($message);
        }
    }
}
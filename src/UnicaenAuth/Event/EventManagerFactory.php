<?php

namespace UnicaenAuth\Event;

use Psr\Container\ContainerInterface;

/**
 * Description of EventManagerFactory
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class EventManagerFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return EventManager
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $shared = $container->has('SharedEventManager') ? $container->get('SharedEventManager') : null;
        $eventManager = new EventManager($shared);

        return $eventManager;
    }
}
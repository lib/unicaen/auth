<?php

namespace UnicaenAuth\Provider\Identity;

use BjyAuthorize\Provider\Identity\ProviderInterface;
use BjyAuthorize\Service\Authorize;
use Laminas\EventManager\EventManagerAwareInterface;
use Laminas\EventManager\EventManagerAwareTrait;
use Laminas\Permissions\Acl\Role\RoleInterface;
use UnicaenAuth\Service\UserContext;

/**
 * Chaîne de responsabilité permettant à plusieures sources de fournir
 * les rôles (ACL) de l'identité authentifiée éventuelle.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 * @see ChainEvent
 * @see \UnicaenAuth\Provider\Identity\ChainServiceFactory
 */
class Chain implements ProviderInterface, EventManagerAwareInterface
{
    use EventManagerAwareTrait;

    /**
     * @var ChainEvent
     */
    protected $event;

    /**
     * @var array
     */
    protected $roles;

    /**
     * @var UserContext
     */
    private $userContextService;

    /**
     * @var Authorize
     */
    private $authorizeService;

    /**
     * @param UserContext $userContextService
     */
    public function setUserContextService(UserContext $userContextService)
    {
        $this->userContextService = $userContextService;
    }

    /**
     * @param Authorize $authorizeService
     */
    public function setAuthorizeService(Authorize $authorizeService)
    {
        $this->authorizeService = $authorizeService;
    }

    /**
     * Retourne lee roles de l'utilisateur courant.
     * Si un rôle courant est sélectionné, c'est lui qu'on retourne.
     *
     * @return string[]|RoleInterface[]
     */
    public function getIdentityRoles()
    {
        $allRoles             = $this->getAllIdentityRoles();
        $selectedIdentityRole = $this->getSelectedIdentityRole();
        $roles                = $selectedIdentityRole ? [$selectedIdentityRole] : $allRoles;

        return $roles;
    }

    /**
     * Retourne l'éventuel rôle courant sélectionné.
     *
     * @return mixed
     */
    private function getSelectedIdentityRole()
    {
        return $this->userContextService->getSelectedIdentityRole();
    }

    /**
     * Collecte tous les rôles de l'utilisateur.
     *
     * @return array
     */
    public function getAllIdentityRoles()
    {
        if (null !== $this->roles) {
            return $this->roles;
        }

        $this->roles = [];

        $e = $this->getEvent();
        $e->setName('getIdentityRoles');
        $e->clearRoles();

        // collecte des rôles
        $this->getEventManager()->triggerEvent($e);
        $roles = $e->getRoles(); /** @var RoleInterface[] $roles */

        foreach ($roles as $role) {
            // ne retient que les rôles déclarés dans les ACL
            if (!$this->authorizeService->getAcl()->hasRole($role)) {
                continue;
            }
            if (is_string($role)) {
                $role = $this->authorizeService->getAcl()->getRole($role);
            }
            // collecte en évitant les doublons
            $this->roles[$role->getRoleId()] = $role;
            // NB : À condition que les 'identity providers' soient dans le bon ordre, cela permet à l'appli
            // de substituer son rôle "Authentifié" maison à celui par défaut.
        }

        return $this->roles;
    }

    /**
     *
     * @return ChainEvent
     */
    public function getEvent()
    {
        if (null === $this->event) {
            $this->event = new ChainEvent();
            $this->event->setTarget($this);
        }
        return $this->event;
    }

    /**
     *
     * @param ChainEvent $event
     * @return self
     */
    public function setEvent(ChainEvent $event)
    {
        $this->event = $event;
        return $this;
    }
}
<?php

namespace UnicaenAuth\Provider\Rule;

use Psr\Container\ContainerInterface;
use UnicaenAuth\Provider\Privilege\PrivilegeProviderInterface;

class PrivilegeRuleProviderFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var PrivilegeProviderInterface $privilegeService */
        $privilegeProvider = $container->get('UnicaenAuth\Privilege\PrivilegeProvider');

        $rules = []; // NB: l'injection des vraies rules est faite par \BjyAuthorize\Service\BaseProvidersServiceFactory

        $instance = new PrivilegeRuleProvider($rules, $container);
        $instance->setPrivilegeProvider($privilegeProvider);

        return $instance;
    }
}
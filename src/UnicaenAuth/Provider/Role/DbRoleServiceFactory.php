<?php

namespace UnicaenAuth\Provider\Role;

use Psr\Container\ContainerInterface;
use UnicaenAuth\Service\RoleService;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * Factory responsible of instantiating {@see \UnicaenAuth\Provider\Role\DbRole}
 */
class DbRoleServiceFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $serviceRole = $container->get('UnicaenAuth\Service\Role');
        /* @var $serviceRole RoleService */

        return new DbRole($serviceRole->getRepo());
    }
}

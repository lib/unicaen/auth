<?php

namespace UnicaenAuth\Provider\Role;

use BjyAuthorize\Exception\InvalidArgumentException;
use Psr\Container\ContainerInterface;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * Factory responsible of instantiating {@see \UnicaenAuth\Provider\Role\Config}
 *
 * @author Marco Pivetta <ocramius@gmail.com>
 */
class ConfigServiceFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('BjyAuthorize\Config');

        if (! isset($config['role_providers']['UnicaenAuth\Provider\Role\Config'])) {
            throw new InvalidArgumentException(
                'Config for "UnicaenAuth\Provider\Role\Config" not set'
            );
        }

        $providerConfig = $config['role_providers']['UnicaenAuth\Provider\Role\Config'];

        /* @var $mapper \UnicaenApp\Mapper\Ldap\Group */
        $mapper = $container->get('ldap_group_mapper');

        $service = new Config($mapper, $providerConfig);

        return $service;
    }
}

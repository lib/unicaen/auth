<?php

namespace UnicaenAuth\Entity\Db;

use Doctrine\ORM\Mapping as ORM;

/**
 * Privilege entity class.
 *
 * @ORM\Entity
 * @ORM\Table(name="privilege")
 */
class Privilege extends AbstractPrivilege
{

}

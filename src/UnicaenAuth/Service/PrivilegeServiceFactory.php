<?php

namespace UnicaenAuth\Service;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;
use Psr\Container\ContainerInterface;
use UnicaenAuth\Entity\Db\Privilege;
use UnicaenAuth\Options\ModuleOptions;

class PrivilegeServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');

        if (! isset($config['unicaen-auth']['privilege_entity_class'])) {
//            throw new InvalidArgumentException("La classe de l'entité privilège n'a pas été trouvée dans la config");
            $config['unicaen-auth']['privilege_entity_class'] = Privilege::class;
        }

        $service = new PrivilegeService();
        $service->setPrivilegeEntityClass($config['unicaen-auth']['privilege_entity_class']);

        /* @var $moduleOptions ModuleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');
        $entityManager = $container->get($moduleOptions->getEntityManagerName());
        $service->setEntityManager($entityManager);

        return $service;
    }
}
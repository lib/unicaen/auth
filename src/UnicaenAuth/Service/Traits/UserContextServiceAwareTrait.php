<?php

namespace UnicaenAuth\Service\Traits;

use UnicaenAuth\Service\UserContext;

/**
 * @author Unicaen
 */
trait UserContextServiceAwareTrait
{
    /**
     * @var UserContext
     */
    protected $serviceUserContext;

    /**
     * @param UserContext $serviceUserContext
     * @return self
     */
    public function setServiceUserContext(UserContext $serviceUserContext)
    {
        $this->serviceUserContext = $serviceUserContext;

        return $this;
    }
}
<?php

namespace UnicaenAuth\Service\Traits;

use UnicaenAuth\Service\ShibService;

trait ShibServiceAwareTrait
{
    /**
     * @var ShibService
     */
    protected $shibService;

    /**
     * @param ShibService $shibService
     */
    public function setShibService(ShibService $shibService)
    {
        $this->shibService = $shibService;
    }
}
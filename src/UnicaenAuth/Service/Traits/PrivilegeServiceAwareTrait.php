<?php

namespace UnicaenAuth\Service\Traits;

use UnicaenAuth\Service\PrivilegeService;
use RuntimeException;

/**
 * Description of PrivilegeServiceAwareTrait
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
trait PrivilegeServiceAwareTrait
{
    /**
     * @var PrivilegeService
     */
    private $servicePrivilege;



    /**
     * @param PrivilegeService $servicePrivilege
     *
     * @return self
     */
    public function setServicePrivilege(PrivilegeService $servicePrivilege)
    {
        $this->servicePrivilege = $servicePrivilege;

        return $this;
    }



    /**
     * @return PrivilegeService
     * @throws RuntimeException
     */
    public function getServicePrivilege()
    {
        return $this->servicePrivilege;
    }
}
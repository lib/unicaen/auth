<?php

namespace UnicaenAuth\Service\Traits;

use UnicaenAuth\Service\RoleService;
use RuntimeException;

/**
 * Description of RoleServiceAwareTrait
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
trait RoleServiceAwareTrait
{
    /**
     * @var RoleService
     */
    private $serviceRole;



    /**
     * @param RoleService $serviceRole
     *
     * @return self
     */
    public function setServiceRole(RoleService $serviceRole)
    {
        $this->serviceRole = $serviceRole;

        return $this;
    }



    /**
     * @return RoleService
     * @throws RuntimeException
     */
    public function getServiceRole()
    {
        return $this->serviceRole;
    }
}
<?php

namespace UnicaenAuth\Service\Traits;

use UnicaenAuth\Service\AuthorizeService;
use RuntimeException;

/**
 * Description of AuthorizeServiceAwareTrait
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
trait AuthorizeServiceAwareTrait
{
    /**
     * @var AuthorizeService
     */
    private $serviceAuthorize;



    /**
     * @param AuthorizeService $serviceAuthorize
     *
     * @return self
     */
    public function setServiceAuthorize(AuthorizeService $serviceAuthorize)
    {
        $this->serviceAuthorize = $serviceAuthorize;

        return $this;
    }



    /**
     * @return AuthorizeService
     * @throws RuntimeException
     */
    public function getServiceAuthorize()
    {
        return $this->serviceAuthorize;
    }
}
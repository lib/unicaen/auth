<?php

namespace UnicaenAuth\Service\Traits;

use UnicaenAuth\Service\CasService;

trait CasServiceAwareTrait
{
    /**
     * @var CasService
     */
    protected $casService;

    /**
     * @param CasService $casService
     */
    public function setCasService(CasService $casService)
    {
        $this->casService = $casService;
    }
}
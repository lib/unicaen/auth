<?php

namespace UnicaenAuth\Service;

use Psr\Container\ContainerInterface;
use UnicaenAuth\Entity\Db\Role;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

class RoleServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');

        if (! isset($config['unicaen-auth']['role_entity_class'])) {
            $config['unicaen-auth']['role_entity_class'] = Role::class;
        }

        $service = new RoleService();
        $service->setRoleEntityClass($config['unicaen-auth']['role_entity_class']);

        return $service;
    }
}
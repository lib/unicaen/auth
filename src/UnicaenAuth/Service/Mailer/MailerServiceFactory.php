<?php

namespace UnicaenAuth\Service\Mailer;

use Psr\Container\ContainerInterface;
use UnicaenApp\Service\Mailer\MailerService;
use UnicaenAuth\Service\UserContext;

/**
 * @author Unicaen
 */
class MailerServiceFactory extends \UnicaenApp\Service\Mailer\MailerServiceFactory
{
    const CURRENT_USER = 'CURRENT_USER';

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): MailerService
    {
        $service = parent::__invoke($container);

        // prise en compte de 'CURRENT_USER' dans les adresses de redirection
        $this->addCurrentUserToRedirection($service, $container);

        return $service;
    }

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function addCurrentUserToRedirection(MailerService $service, ContainerInterface $container): void
    {
        $redirectTo = $service->getRedirectTo();

        // si 'CURRENT_USER' est trouvé dans les éventuelles adresses de redirection des mails,
        // on va ajouter l'email de l'utilisateur connecté aux destinataires des redirections...

        if (!in_array(static::CURRENT_USER, $redirectTo)) {
            return;
        }

        // déterminaiton de l'utilisateur authentifié
        /** @var \UnicaenAuth\Service\UserContext $userContext */
        $userContext = $container->get(UserContext::class);
        if ($userContext->isUsurpationEnCours()) {
            $user = $userContext->getUsurpateurEnCours();
        } else {
            $user = $userContext->getDbUser();
        }

        // si un utilisateur est authentifié, on ajoute son adresse mail (si pas déjà présente)
        if ($user !== null) {
            if (!in_array($email = $user->getEmail(), $redirectTo) && !array_key_exists($email, $redirectTo)) {
                $redirectTo[$email] = $user->getDisplayName();
            }
        }

        // retrait de 'CURRENT_USER' de la liste finale
        $redirectTo = array_diff($redirectTo, [static::CURRENT_USER]);

        $service->setRedirectTo($redirectTo);
    }
}

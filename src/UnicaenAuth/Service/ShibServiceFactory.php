<?php

namespace UnicaenAuth\Service;

use Psr\Container\ContainerInterface;
use UnicaenAuth\Options\ModuleOptions;

class ShibServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');

        $service = new ShibService();
        $service->setShibbolethConfig($moduleOptions->getShib());
        $service->setUsurpationAllowedUsernames($moduleOptions->getUsurpationAllowedUsernames());

        return $service;
    }
}
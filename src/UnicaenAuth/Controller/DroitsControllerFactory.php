<?php

namespace UnicaenAuth\Controller;

use Psr\Container\ContainerInterface;
use UnicaenAuth\Form\Droits\RoleForm;
use UnicaenAuth\Service\PrivilegeService;
use UnicaenAuth\Service\RoleService;

class DroitsControllerFactory
{
    /**
     * @param ContainerInterface $container
     * @return DroitsController
     */
    public function __invoke(ContainerInterface $container)
    {
        /** @var RoleService $serviceRole */
        $serviceRole = $container->get('UnicaenAuth\Service\Role');
        /** @var RoleForm $formDroitsRole */
        $formDroitsRole = $container->get('FormElementManager')->get('UnicaenAuth\Form\Droits\Role');
        /** @var PrivilegeService $servicePrivilege */
        $servicePrivilege = $container->get('UnicaenAuth\Service\Privilege');

        $controller = new DroitsController();
        $controller->setServiceRole($serviceRole);
        $controller->setFormDroitsRole($formDroitsRole);
        $controller->setServicePrivilege($servicePrivilege);

        return $controller;
    }
}
<?php

namespace UnicaenAuth\Controller;

use Psr\Container\ContainerInterface;
use UnicaenAuth\Form\LoginForm;
use UnicaenAuth\Options\ModuleOptions;
use UnicaenAuth\Service\ShibService;
use UnicaenAuth\Service\User as UserService;
use UnicaenAuth\Service\UserContext;
use ZfcUser\Controller\RedirectCallback;

class AuthControllerFactory
{
    /**
     * @param ContainerInterface $container
     * @return AuthController
     */
    public function __invoke(ContainerInterface $container): AuthController
    {
        /** @var ShibService $shibService */
        $shibService = $container->get(ShibService::class);

        /* @var $userService UserService */
        $userService = $container->get('unicaen-auth_user_service');

        /* @var $userContextService UserContext */
        $userContextService = $container->get(UserContext::class);

        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');

        /* @var RedirectCallback $redirectCallback */
        $redirectCallback = $container->get('zfcuser_redirect_callback');

        $controller = new AuthController();
        $controller->setShibService($shibService);
        $controller->setUserService($userService);
        $controller->setServiceUserContext($userContextService);
        $controller->setModuleOptions($moduleOptions);
        $controller->setRedirectCallback($redirectCallback);

        $authTypesConfig = $moduleOptions->getEnabledAuthTypes();
        foreach ($authTypesConfig as $type => $config) {
            /** @var LoginForm $loginForm */
            $loginForm = $container->get($config['form']);
            $controller->addLoginForm($loginForm);
        }

        return $controller;
    }
}
<?php

namespace UnicaenAuth\View\Helper;

use Psr\Container\ContainerInterface;
use UnicaenAuth\Options\ModuleOptions;

class CasConnectViewHelperFactory
{
    /**
     * @param ContainerInterface $container
     * @return CasConnectViewHelper
     */
    public function __invoke(ContainerInterface $container)
    {
        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');
        $config = $moduleOptions->getCas();

        $enabled = isset($config['enabled']) && (bool) $config['enabled'];
        $description = $config['description'] ?? null;

        $helper = new CasConnectViewHelper();
        $helper->setEnabled($enabled);
        $helper->setDescription($description);

        return $helper;
    }
}
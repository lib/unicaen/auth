<?php
namespace UnicaenAuth\View\Helper;

/**
 * Aide de vue affichant toutes les infos concernant l'utilisateur connecté.
 * C'est à dire le nom de l'utilisateur connecté & son rôle courant.
 *
 * Lorsqu'on clique sur le nom, s'affiche un popover
 * présentant les rôles sélectionnables, des infos administratives, le formulaire d'usurpation (si habilité)
 * et le lien de déconnexion.
 *
 * Si aucun utilisateur n'est connecté, cette aide de vue renvoit ''.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class UserCurrent extends UserAbstract
{
    /**
     * @var bool
     */
    protected $affectationFineSiDispo = false;

    /**
     * Point d'entrée.
     * 
     * @param boolean $affectationFineSiDispo Indique s'il faut prendre en compte l'affectation
     * plus fine (ucbnSousStructure) si elle existe, à la place de l'affectation standard (niveau 2)
     * @return self 
     */
    public function __invoke($affectationFineSiDispo = false)
    {
        $this->setAffectationFineSiDispo($affectationFineSiDispo);
        return $this;
    }
    
    /**
     * Retourne le code HTML généré par cette aide de vue.
     * 
     * @return string 
     */
    public function __toString(): string
    {
        if (! $this->getIdentity()) {
            return '';
        }

        //
        // Sous la forme d'un lien :
        //
        // - Nom de l'utilisateur connecté, le cas échéant.
        /* @var $userStatusHelper UserStatus */
        $userStatusHelper = $this->view->plugin('userStatus');
        $nom = $userStatusHelper(false);
        // - Rôle courant.
        $role = '';
        $userProfileSelectable = true;
        if ($userProfileSelectable) {
            $role = $this->getUserContext()->getSelectedIdentityRole();
            // cas où aucun rôle n'est sélectionné : on affiche le 1er rôle sélectionnable ou sinon "user"
            if ($role === null) {
                $selectableRoles = $this->getUserContext()->getSelectableIdentityRoles();
                $role = current($selectableRoles) ?: $this->getUserContext()->getIdentityRole('user');
            }
            $role = sprintf(
                "<br/><small class='role-libelle'>%s</small>",
                !method_exists($role, '__toString') ? $role->getRoleId() : $role
            );
        }

        //
        // Dans un popover :
        //
        // - Rôles sélectionnables.
        /* @var \UnicaenAuth\View\Helper\UserProfile $userProfileHelper */
        $userProfileHelper = $this->view->plugin('userProfile');
        $userProfileHelper->setUserProfileSelectable($userProfileSelectable);
        $roles = (string) $userProfileHelper;
        // - Infos administratives.
        /* @var \UnicaenAuth\View\Helper\UserInfo $userInfoHelper */
        $userInfoHelper = $this->view->plugin('userInfo');
        $infos = (string) $userInfoHelper($this->getAffectationFineSiDispo());
        // - Usurpation d'identité.
        /* @var \UnicaenAuth\View\Helper\UserUsurpationHelper $userUsurpationHelper */
        $userUsurpationHelper = $this->view->plugin('userUsurpation');
        $usurpation = (string) $userUsurpationHelper;
        // - Lien de déconnexion.
        /** @var \UnicaenAuth\View\Helper\UserConnection $userConnectionHelper */
        $userConnectionHelper = $this->getView()->plugin('userConnection');
        $deconnexion = $userConnectionHelper->addClass('btn btn-lg btn-outline-success')->renderDisconnection();

        $linkText = $nom . $role;
        $popoverContent = htmlspecialchars(preg_replace('/\r\n|\n|\r/', '',
            $roles .
            $infos .
            $usurpation .
            '<hr>' .
            '<div class="text-center">' . $deconnexion . '</div>'
        ));

        return <<<EOS
<a class="navbar-link dropdown-toggle"
   id="user-current-info" 
   data-bs-placement="bottom" 
   data-bs-toggle="popover" 
   data-bs-container="#navbar" 
   data-bs-html="true" 
   data-bs-sanitize="false" 
   data-bs-content="$popoverContent" 
   onclick="return false"
   href="#">$linkText<span class="caret"></span></a>
EOS;
    }

    /**
     * Indique si l'affichage de l'affectation fine éventuelle est activé ou non.
     * 
     * @return bool
     */
    public function getAffectationFineSiDispo()
    {
        return $this->affectationFineSiDispo;
    }

    /**
     * Active ou non l'affichage de l'affectation fine éventuelle.
     * 
     * @param bool $affectationFineSiDispo
     * @return self
     */
    public function setAffectationFineSiDispo($affectationFineSiDispo = true)
    {
        $this->affectationFineSiDispo = $affectationFineSiDispo;
        return $this;
    }
}

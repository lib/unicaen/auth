<?php

namespace UnicaenAuth\View\Helper;

use Laminas\View\Renderer\PhpRenderer;

/**
 * Aide de vue dessinant le bouton de connexion via Shibboleth,
 * si l'authentification Shibboleth est activée.
 *
 * @method PhpRenderer getView()
 * @author Unicaen
 */
class ShibConnectViewHelper extends AbstractConnectViewHelper
{
    public function __construct()
    {
        $this->setType('shib');
        $this->setTitle("Via la fédération d'identité");
    }
}
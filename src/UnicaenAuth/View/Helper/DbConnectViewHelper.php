<?php

namespace UnicaenAuth\View\Helper;

use Laminas\View\Renderer\PhpRenderer;

/**
 * Aide de vue dessinant le formulaire d'authentification locale,
 * si l'authentification locale est activée.
 *
 * @method PhpRenderer getView()
 * @author Unicaen
 * @deprecated Use {@see LocalConnectViewHelper}
 */
class DbConnectViewHelper extends AbstractConnectViewHelper
{
    public function __construct()
    {
        $this->setType('db');
        $this->setTitle("Avec un compte local");
    }
}
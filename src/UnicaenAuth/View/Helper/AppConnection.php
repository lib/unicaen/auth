<?php

namespace UnicaenAuth\View\Helper;

/**
 * Aide de vue dessinant :
 * - le nom de l'utilisateur connecté éventuel ;
 * - le lien de connexion/déconnexion.
 *
 * @author Unicaen
 */
class AppConnection extends \UnicaenApp\View\Helper\AppConnection
{
    /**
     * @return string
     */
    public function __toString(): string
    {
        $parts = [];

        /** @var \UnicaenAuth\View\Helper\UserCurrent $userCurrentHelper */
        $userCurrentHelper = $this->getView()->plugin('userCurrent');
        if ($userCurrentHelper) {
            if ($html = "" . $userCurrentHelper) {
                $parts[] = $html;
            }
        }

        /** @var \UnicaenAuth\View\Helper\UserConnection $userConnectionHelper */
        $userConnectionHelper = $this->getView()->plugin('userConnection');
        if ($userConnectionHelper) {
            // On ne dessine que le lien de Connexion.
            // Le lien de déconnexion est dessiné dans le popover dédié à l'utilisateur connecté.
            if ($html = $userConnectionHelper->addClass('btn btn-success')->renderConnection()) {
                $parts[] = $html;
            }
        }

        return implode(' | ', $parts);
    }
}
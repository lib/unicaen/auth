<?php

namespace UnicaenAuth\View\Helper;

use Laminas\View\Renderer\PhpRenderer;

/**
 * Aide de vue dessinant le formulaire d'authentification locale (ldap ou db),
 * si l'authentification locale est activée.
 *
 * @method PhpRenderer getView()
 * @author Unicaen
 */
class LocalConnectViewHelper extends AbstractConnectViewHelper
{
    public function __construct()
    {
        $this->setType('local');
        $this->setTitle("Avec un compte local");
    }
}
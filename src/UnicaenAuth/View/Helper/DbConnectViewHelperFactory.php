<?php

namespace UnicaenAuth\View\Helper;

use Psr\Container\ContainerInterface;
use UnicaenAuth\Options\ModuleOptions;

class DbConnectViewHelperFactory
{
    /**
     * @param ContainerInterface $container
     * @return DbConnectViewHelper
     */
    public function __invoke(ContainerInterface $container)
    {
        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');
        $config = $moduleOptions->getDb();

        $enabled = isset($config['enabled']) && (bool) $config['enabled'];
        $description = $config['description'] ?? null;

        $helper = new DbConnectViewHelper();
        $helper->setEnabled($enabled);
        $helper->setDescription($description);
        $helper->setPasswordReset(true);

        return $helper;
    }
}
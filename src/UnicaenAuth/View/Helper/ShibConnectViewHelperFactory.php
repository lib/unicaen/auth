<?php

namespace UnicaenAuth\View\Helper;

use Psr\Container\ContainerInterface;
use UnicaenAuth\Options\ModuleOptions;

class ShibConnectViewHelperFactory
{
    /**
     * @param ContainerInterface $container
     * @return ShibConnectViewHelper
     */
    public function __invoke(ContainerInterface $container)
    {
        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');
        $config = $moduleOptions->getShib();

        $enabled = isset($config['enabled']) && (bool) $config['enabled'];
        $description = $config['description'] ?? null;

        $helper = new ShibConnectViewHelper();
        $helper->setEnabled($enabled);
        $helper->setDescription($description);

        return $helper;
    }
}
<?php

namespace UnicaenAuth\View\Helper;

use Laminas\Form\Form;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Renderer\PhpRenderer;

/**
 * Aide de vue dessinant le formulaire correspondant au type d'authentification spécifié.
 *
 * @property PhpRenderer $view
 * @author Unicaen
 */
class ConnectViewHelper extends AbstractHelper
{
    /**
     * @param string $type 'local', 'shib', ldap', etc.
     * @param Form $form
     * @return AbstractConnectViewHelper
     */
    public function __invoke(string $type, Form $form): AbstractConnectViewHelper
    {
        return $this->view->plugin($type . 'Connect')($form); // ex: 'localConnect'
    }
}
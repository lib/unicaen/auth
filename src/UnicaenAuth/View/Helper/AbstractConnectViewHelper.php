<?php

namespace UnicaenAuth\View\Helper;

use Laminas\Form\Form;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;

/**
 * Aide de vue dessinant un formulaire d'authentification d'un type particulier,
 * si l'authentification de ce type est activée.
 *
 * @method PhpRenderer getView()
 * @author Unicaen
 */
class AbstractConnectViewHelper extends AbstractHelper
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var bool
     */
    protected $enabled = true;

    /**
     * @var bool
     */
    protected $passwordReset = false;

    /**
     * @var Form
     */
    protected $form;

    /**
     * @param string $type
     * @return AbstractConnectViewHelper
     */
    public function setType(string $type): AbstractConnectViewHelper
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param bool $enabled
     * @return $this
     */
    public function setEnabled($enabled = true)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title): AbstractConnectViewHelper
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param string|null $description
     * @return AbstractConnectViewHelper
     */
    public function setDescription(?string $description): AbstractConnectViewHelper
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @param bool $passwordReset
     * @return $this
     */
    public function setPasswordReset(bool $passwordReset): AbstractConnectViewHelper
    {
        $this->passwordReset = $passwordReset;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @return bool
     */
    public function isPasswordReset(): bool
    {
        return $this->passwordReset;
    }

    /**
     * @param Form $form
     * @return $this
     */
    public function __invoke(Form $form)
    {
        $this->form = $form;

        $this->getView()->resolver()->attach(
            new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]])
        );

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        try {
            return $this->getView()->render("connect", [
                'type' => $this->type,
                'title' => $this->title,
                'description' => $this->description,
                'enabled' => $this->enabled,
                'form' => $this->form,
                'redirect' => null,
                'passwordReset' => $this->passwordReset,
            ]);
        } catch (\Exception $e) {
            return '<p>' . $e->getMessage() . '</p><p>' . $e->getTraceAsString() . '</p>';
        }
    }
}
<?php

namespace UnicaenAuth\Mouchard;

use UnicaenApp\Mouchard\MouchardCompleterInterface;
use UnicaenApp\Mouchard\MouchardMessage;
use UnicaenAuth\Service\Traits\UserContextServiceAwareTrait;

/**
 * Interface MouchardCompleterInterface
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 * @package UnicaenApp\Mouchard
 */
class MouchardCompleterAuth implements MouchardCompleterInterface
{
    use UserContextServiceAwareTrait;

    /**
     * @param MouchardMessage $message
     * @return $this
     */
    public function complete(MouchardMessage $message)
    {
        $user = $this->serviceUserContext->getDbUser();
        if ($user){
            $message->setUser($user->getDisplayName());
            $message->setLogin($user->getUsername());
        }

        $message->setRole((string)$this->serviceUserContext->getSelectedIdentityRole());

        return $this;
    }

}
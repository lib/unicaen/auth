<?php
namespace UnicaenAuthTest\Authentication\Adapter;

use PHPUnit_Framework_TestCase;
use UnicaenApp\Mapper\Ldap\People;
use UnicaenAuth\Authentication\Adapter\AbstractFactory;
use UnicaenAuth\Service\User;
use Laminas\EventManager\EventManager;
use Laminas\EventManager\EventManagerAwareInterface;
use Laminas\ServiceManager\ServiceManager;
use UnicaenApp\Exception\LogicException;
use ZfcUser\Options\ModuleOptions;

/**
 * Description of AbstractFactoryTest
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class AbstractFactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var AbstractFactory
     */
    protected $factory;

    protected function setUp()
    {
        $this->factory = new AbstractFactory();
    }

    public function getInvalidServiceClassName()
    {
        return [
            'unknown-class'   => ['UnicaenAuth\Authentication\Adapter\Xxxx'],
            'wrong-namespace' => ['Any\Other\Namespaced\Class'],
        ];
    }

    /**
     * @dataProvider getInvalidServiceClassName
     * @param string $serviceClassName
     */
    public function testCanRefuseCreatingServiceWithInvalidName($serviceClassName)
    {
        $this->assertFalse($this->factory->canCreateServiceWithName(new ServiceManager(), null, $serviceClassName));
    }

    public function getValidServiceClassName()
    {
        return [
            'cas'  => ['UnicaenAuth\Authentication\Adapter\Cas'],
            'db'   => ['UnicaenAuth\Authentication\Adapter\Db'],
            'ldap' => ['UnicaenAuth\Authentication\Adapter\Ldap'],
        ];
    }

    /**
     * @dataProvider getValidServiceClassName
     * @param string $serviceClassName
     */
    public function testCanAcceptCreatingServiceWithValidName($serviceClassName)
    {
        $this->assertTrue($this->factory->canCreateServiceWithName(new ServiceManager(), null, $serviceClassName));
    }

    /**
     * @dataProvider getInvalidServiceClassName
     * @expectedException \UnicaenApp\Exception\LogicException
     * @param string $serviceClassName
     */
    public function testCreateServiceWithNameThrowsExceptionIfInvalidServiceSpecified($serviceClassName)
    {
        $this->factory->createServiceWithName(new ServiceManager(), null, $serviceClassName);
    }

    /**
     * @dataProvider getValidServiceClassName
     * @param string $serviceClassName
     */
    public function testCanCreateServiceWithName($serviceClassName)
    {
        $eventManager = new EventManager();

        $serviceLocator = $this->createMock('Laminas\ServiceManager\ServiceManager'/*, ['get']*/);
        $serviceLocator->expects($this->any())
                       ->method('get')
                       ->will($this->returnCallback(function($serviceName) use ($eventManager) {
                           if ('unicaen-auth_user_service' === $serviceName) {
                               return new User();
                           }
                           if ('EventManager' === $serviceName) {
                               return $eventManager;
                           }
                           if ('zfcuser_module_options' === $serviceName) {
                               return new ModuleOptions();
                           }
                           if ('unicaen-app_module_options' === $serviceName) {
                               return new \UnicaenApp\Options\ModuleOptions();
                           }
                           if ('unicaen-auth_module_options' === $serviceName) {
                               return new \UnicaenAuth\Options\ModuleOptions();
                           }
                           if ('ldap_people_mapper' === $serviceName) {
                               return new People();
                           }
                           return null;
                       }));

        $adapter = $this->factory->createServiceWithName($serviceLocator, null, $serviceClassName);

        $this->assertInstanceOf($serviceClassName, $adapter);

        if ($adapter instanceof EventManagerAwareInterface) {
            $this->assertSame($eventManager, $adapter->getEventManager());
        }
    }
}
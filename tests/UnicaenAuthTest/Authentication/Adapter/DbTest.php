<?php
namespace UnicaenAuthTest\Authentication\Adapter;

use PDOException;
use PHPUnit_Framework_TestCase;
use UnicaenAuth\Authentication\Adapter\Db;
use UnicaenAuth\Options\ModuleOptions;
use Laminas\EventManager\EventInterface;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\ServiceManager;
use Laminas\Stdlib\Parameters;
use ZfcUser\Authentication\Adapter\AdapterChainEvent;
use ZfcUser\Mapper\User;

/**
 * Description of DbTest
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class DbTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Db|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $adapter;

    protected $moduleOptions;

    /**
     * @var User|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $mapper;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->moduleOptions = $moduleOptions = new ModuleOptions([
            'cas' => [
                'connection' => [
                    'default' => [
                        'params' => [
                            'hostname' => 'cas.unicaen.fr',
                            'port' => 443,
                            'version' => "2.0",
                            'uri' => "",
                            'debug' => false,
                        ],
                    ],
                ],
            ],
        ]);

        $this->mapper = $mapper = $this->createMock('ZfcUser\Mapper\User'/*, ['findByUsername', 'findByEmail']*/);

        /** @var ServiceManager|\PHPUnit_Framework_MockObject_MockObject $serviceManager */
        $serviceManager = $this->createMock('Laminas\ServiceManager\ServiceManager'/*, ['get']*/);
        $serviceManager->expects($this->any())
                       ->method('get')
                       ->will($this->returnCallback(function($serviceName) use ($moduleOptions, $mapper) {
                           if ('zfcuser_module_options' === $serviceName) {
                               return new \ZfcUser\Options\ModuleOptions();
                           }
                           if ('unicaen-auth_module_options' === $serviceName) {
                               return $moduleOptions;
                           }
                           if ('zfcuser_user_mapper' === $serviceName) {
                               return $mapper;
                           }
                           return null;
                       }));

        $this->adapter = new Db();
        $this->adapter->setServiceManager($serviceManager);
    }

    public function getException()
    {
        return [
            //[new PDOException()],
            [new ServiceNotFoundException()],
        ];
    }

    /**
     * @dataProvider getException
     * @param \Exception $exception
     */
    public function testAuthenticateReturnsFalseIfExceptionThrown($exception)
    {
        $this->mapper->expects($this->once())
                     ->method($this->logicalOr('findByUsername', 'findByEmail'))
                     ->will($this->throwException($exception));

        $request = new Request();
        $request->setPost(new Parameters(['identity' => 'bob', 'credential' => "xxxxx"]));

        /** @var EventInterface|\PHPUnit_Framework_MockObject_MockObject $event */
        $event = $this->createMock(EventInterface::class);
        $event
            ->expects($this->exactly(2))
            ->method('getTarget')
            ->willReturn((new AdapterChainEvent())->setRequest($request));

        $result = $this->adapter->authenticate($event);
        $this->assertFalse($result);
    }

    public function testAuthenticateReturnsParentMethodResult()
    {
        $request = new Request();
        $request->setPost(new Parameters(['identity' => 'bob', 'credential' => "xxxxx"]));

        /** @var EventInterface|\PHPUnit_Framework_MockObject_MockObject $event */
        $event = $this->createMock(EventInterface::class);
        $event
            ->expects($this->exactly(2))
            ->method('getTarget')
            ->willReturn((new AdapterChainEvent())->setRequest($request));

        $result = $this->adapter->authenticate($event);
        $this->assertFalse($result);
    }
}
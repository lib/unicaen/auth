<?php

namespace UnicaenAuthTest\Provider\Identity;

/**
 * Description of LdapServiceFactoryTest
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class LdapServiceFactoryTest extends BaseServiceFactoryTest
{
    protected $factoryClass = 'UnicaenAuth\Provider\Identity\LdapServiceFactory';
    protected $serviceClass = 'UnicaenAuth\Provider\Identity\Ldap';
}
<?php

namespace UnicaenAuthTest\Provider\Identity;

/**
 * Description of DbServiceFactoryTest
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class DbServiceFactoryTest extends BaseServiceFactoryTest
{
    protected $factoryClass = 'UnicaenAuth\Provider\Identity\DbServiceFactory';
    protected $serviceClass = 'UnicaenAuth\Provider\Identity\Db';
}
<?php
// Application config
$appConfig = [];//include __DIR__ . '/../../../../../config/application.config.php';

// Test config
$testConfig = [
    'modules' => [
        'UnicaenAuth'
    ],
    'module_listener_options' => [
        'config_glob_paths'    => [
            __DIR__ . '/autoload/{,*.}{global,local}.php',
        ],
    ],
];

return \Laminas\Stdlib\ArrayUtils::merge($appConfig, $testConfig);

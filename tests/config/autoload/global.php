<?php
return [
//    'translator' => array(
//        'locale' => 'fr_FR',
//        'translation_file_patterns' => array(
//            array(
//                'type'     => 'getdtext',
//                'base_dir' => __DIR__ . '/../language',
//                'pattern'  => '%s.mo',
//            ),
//        ),
//    ),
    'controllers' => [
        'invokables' => [
            'UnicaenAppTest\Controller\Plugin\TestAsset\Contact' => 'UnicaenAppTest\Controller\Plugin\TestAsset\ContactController',
        ],
    ],
];

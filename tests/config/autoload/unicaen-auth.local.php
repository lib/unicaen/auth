<?php
/**
 * UnicaenAuth Configuration
 *
 * If you have a ./config/autoload/ directory set up for your project, you can
 * drop this config file in it and change the values as you wish.
 */
$settings = [
    /**
     * Paramètres de connexion au serveur CAS.
     */
    'cas' => [
//        'enabled' => true,
//        'connection' => array(
//            'default' => array(
//                'params' => array(
//                    'hostname' => 'cas.unicaen.fr',
//                    'port' => 443,
//                    'version' => "2.0",
//                    'uri' => "",
//                    'debug' => false,
//                ),
//            ),
//        ),
    ],
    /**
     * Identifiants de connexion LDAP autorisés à faire de l'usurpation d'identité.
     * NB: à réserver exclusivement aux tests.
     */
//    'usurpation_allowed_usernames' => array(),
];

/**
 * You do not need to edit below this line
 */
return [
    'unicaen-auth' => $settings,
];
<?php

return [
    'unicaen-auth' => [

        /**
         * Configuration de l'authentification centralisée (CAS).
         */
        'cas' => [
            /**
             * Ordre d'affichage du formulaire de connexion.
             */
            'order' => 1,

            /**
             * Activation ou non de ce mode d'authentification.
             */
            'enabled' => true,

            /**
             * Description facultative de ce mode d'authentification qui apparaîtra sur la page de connexion.
             */
            'description' => "Cliquez sur le bouton ci-dessous pour accéder à l'authentification centralisée.",

            /**
             * Infos de connexion au serveur CAS.
             */
            'connection' => [
                'default' => [
                    'params' => [
                        'hostname' => 'host.domain.fr',
                        'port'     => 443,
                        'version'  => "2.0",
                        'uri'      => "",
                        'debug'    => false,
                    ],
                ],
            ]
        ],

        /**
         * Configuration de l'authentification locale (compte LDAP établissement, ou compte BDD application).
         */
        'local' => [
            'order' => 2,
            'enabled' => true,
            'description' => "Utilisez ce formulaire si vous possédez un compte LDAP établissement ou un compte local dédié à l'application.",

            /**
             * Mode d'authentification à l'aide d'un compte dans la BDD de l'application.
             */
            'db' => [
                'enabled' => true, // doit être activé pour que l'usurpation fonctionne (cf. Authentication/Storage/Db::read()) :-/
            ],
            /**
             * Mode d'authentification à l'aide d'un compte LDAP.
             */
            'ldap' => [
                'enabled' => true,

                /**
                 * Activation ou non des logs (via `error_log` par défaut) à propos des échecs d'authentification LDAP.
                 */
                'log_failures' => false, /** @see \UnicaenAuth\Event\Listener\LdapAuthenticationFailureLoggerListener */
            ],
        ],

        /**
         * Authentification via la fédération d'identité (Shibboleth).
         */
        'shib' => [
            'order' => 3,
            'enabled' => false,
            'description' =>
                "Cliquez sur le bouton ci-dessous pour accéder à l'authentification via la fédération d'identité. " .
                "<strong>NB: Vous devrez utiliser votre compte " .
                "&laquo; <a href='http://vie-etudiante.unicaen.fr/vie-numerique/etupass/'>etupass</a> &raquo; " .
                "pour vous authentifier...</strong>",

            /**
             * URL de déconnexion.
             */
            'logout_url' => '/Shibboleth.sso/Logout?return=', // NB: '?return=' semble obligatoire!

            /**
             * Simulation d'authentification d'un utilisateur.
             */
            //'simulate' => [
            //    'eppn'        => 'eppn@domain.fr',
            //    'supannEmpId' => '00012345',
            //],

            /**
             * Alias éventuels des clés renseignées par Shibboleth dans la variable superglobale $_SERVER
             * une fois l'authentification réussie.
             */
            'aliases' => [
                'eppn'                   => 'HTTP_EPPN',
                'mail'                   => 'HTTP_MAIL',
                'eduPersonPrincipalName' => 'HTTP_EPPN',
                'supannEtuId'            => 'HTTP_SUPANNETUID',
                'supannEmpId'            => 'HTTP_SUPANNEMPID',
                'supannCivilite'         => 'HTTP_SUPANNCIVILITE',
                'displayName'            => 'HTTP_DISPLAYNAME',
                'sn'                     => 'HTTP_SN',
                'givenName'              => 'HTTP_GIVENNAME',
            ],

            /**
             * Clés dont la présence sera requise par l'application dans la variable superglobale $_SERVER
             * une fois l'authentification réussie.
             */
            //'required_attributes' => [
            //    'eppn',
            //    'mail',
            //    'eduPersonPrincipalName',
            //    'supannCivilite',
            //    'displayName',
            //    'sn|surname', // i.e. 'sn' ou 'surname'
            //    'givenName',
            //    'supannEtuId|supannEmpId',
            //],

            /**
             * Configuration de la stratégie d'extraction d'un identifiant utile parmi les données d'authentification
             * shibboleth.
             * Ex: identifiant de l'usager au sein du référentiel établissement, transmis par l'IDP via le supannRefId.
             */
            'shib_user_id_extractor' => [
                // domaine (ex: 'unicaen.fr') de l'EPPN (ex: hochonp@unicaen.fr')
//                'unicaen.fr' => [
//                    'supannRefId' => [
//                        // nom du 1er attribut recherché
//                        'name' => 'supannRefId', // ex: '{OCTOPUS:ID}1234;{ISO15693}044D1AZE7A5P80'
//                        // pattern éventuel pour extraire la partie intéressante
//                        'preg_match_pattern' => '|\{OCTOPUS:ID\}(\d+)|', // ex: permet d'extraire '1234'
//                    ],
//                    'supannEmpId' => [
//                        // nom du 2e attribut recherché
//                        'name' => 'supannEmpId',
//                        // pas de pattern donc valeur brute utilisée
//                        'preg_match_pattern' => null,
//                    ],
//                    'supannEtuId' => [
//                        // nom du 3e attribut recherché
//                        'name' => 'supannEtuId',
//                    ],
//                ],
                // config de repli pour tous les autres domaines
                'default' => [
                    'supannEmpId' => [
                        'name' => 'supannEmpId',
                    ],
                    'supannEtuId' => [
                        'name' => 'supannEtuId',
                    ],
                ],
            ],
        ],

        /**
         * Identifiants de connexion autorisés à faire de l'usurpation d'identité.
         * (NB: à réserver exclusivement aux tests.)
         */
        'usurpation_allowed_usernames' => [
            //'username', // format LDAP
            //'e.mail@domain.fr', // format BDD
            //'eppn@domain.fr', // format Shibboleth
        ],
    ],
];

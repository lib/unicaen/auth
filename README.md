# UnicaenAuth

Ce module :

- ajoute à une application la possibilité d'identifier/authentifier l'utilisateur (LDAP, base de données ou CAS).
- fournit la possibilité à l'utilisateur de se créer un compte dans la base de données de l'application (option de config).
- fournit les fonctionnalités d'habilitation de l'utilisateur (ACL).
- Une bibliothèque de rôles éditable via une IHM
- Un système de gestion des droits avec des privilèges éditables via une IHM
- Un système d'assertions avancées pour gérer des cas complexes d'autorisations
- requiert les modules suivants :
  - UnicaenApp
  - ZfcUserDoctrineOrm
  - phpCAS
  - BjyAuthorize

## Documentation

- [Installation](./doc/installation.md)
- [Configuration](./doc/configuration.md)
- [Authentification](./doc/authentification.md)
- [Services](./doc/services.md)
- [Utilisation de la gestion des droits et privilèges](./doc/droits.md)
- [Aides de vue (view helpers)](./doc/helpers.md)

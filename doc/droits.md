# Gestion des droits

## Généralités

Dans UnicaenAuth, les droits sont gérés (par défaut) de la manière suivante :

![alt text][systeme_de_gestion_des_droits]

### Utilisateurs

L'utilisateur s'authentifie. Une fois cela fait, 0 à n rôles lui sont associés (en base de données ou bien avec le LDAP). Si plusieurs sont possibles alors il pourra choisir d'en endosser un (en cliquant sur son nom en haut à droite de l'interface, une liste lui est proposée le cas échéant).

### Rôles

Les rôles sont définis dans la base de données (table user_role). Ils sont dynamiques, c'est-à-dire que rien dans le code de l'application ne doit faire référence à un rôle en particulier, hormis les rôles spéciaux guest et user (respectivement non authentifié et authentifié : les rôles de base).

Certains rôles sont proposés par défaut (gestionnaire, administrateur, etc). L'administrateur est par défaut le seul à accéder à la gestion des rôles et des privilèges.

Les rôles peuvent être gérés depuis une interface graphique. Elle est accessible si vous êtes administrateur (par défaut), sur “Droits d'accès ⇒ Rôles”.

Exemples de rôle :
- Administrateur
- Gestionnaire
- Responsable
- Personnel
- Etudiant

#### Visibilité des rôles depuis l'extérieur

Il est parfois nécessaire de limiter l'accès à certains rôles pouvant traiter des données sensibles.
La paramètre "Accessible de l'extérieur" précise cela. S'il est égal à oui (choix par défaut), alors pas de restriction. 
Dans le cas contraire le rôle ne sera disponible pour l'utilisateur que si ce dernier se connecte depuis le réseau de l'université de Caen (paramètre par défaut).
Par exemple, si une personne est à la fois administrateur et simple utilisateur de votre application et que le rôle admin n'est pas accessible de l'extérieur, alors
de chez lui ou bien depuis EDUROAM (qui est considéré comme extérieur à l'établissement), il ne pourra se connecter qu'en tant que simple utilisateur sans pouvoir
switcher vers admin. 

Pour plus d'indication sur la manière de détecter si l'on se situe sur le réseau de l'établissement ou non, aller sur 
[https://git.unicaen.fr/lib/unicaen/app/blob/master/doc/Services.md#hostlocalization](https://git.unicaen.fr/lib/unicaen/app/blob/master/doc/Services.md#hostlocalization).

### Privilèges

Les privilèges représentent des fonctionnalités de votre logiciel (édition d'un contrat, visualisation d'un tableau de bord particulier, export CSV particulier, etc).

La liste des privilèges doit être renseignés dans la base de données (table privilege). Chaque privilège appartient à une catégorie, ce qui permet de mieux les identifier. Les catégories doivent être également créées par vos soins.

*Le système de gestion des droits gère lui-même ses accès, de la même manière que n'importe quelle autre fonctionnalité de votre logiciel.*

Exemples de privilège :

Catégorie | Privilège
--------- | -------------
Album     | Visualisation
Album     | Édition

### Utilisateurs => Rôles

Le lien entre utilisateur et rôle peut se faire de deux manières :

    Soit l'association entre utilisateur et rôle est renseignée directement dans la base de données (table user/role/linker
    Soit en fonction du filtre LDAP du rôle : si un rôle a un filtre LDAP renseigné et que le compte LDAP correspond à ce filtre alors il peut être associé au rôle.


### Rôles => privilèges

Le lien entre les rôles et les privilèges se fait au moyen de la table role_privilege. Une interface graphique permet d'éditer ces associations. Elle est accessible si vous êtes administrateur (par défaut), sur “Droits d'accès ⇒ Privilèges”.

## Interface

Voici un aperçu de l'IHM qui est proposée :

![alt text][roles]

Il est ici possible de visualiser la liste des rôles et de l'éditer. Vous avez également un exemple de filtres LDAP pour les étudiants et le personnel.

Et voici le tableau croisé des privilèges et des rôles.

![alt text][privileges]

Ces deux vues peuvent être accessibles en lecture seule uniquement, en fonction des privilèges accordés.

## Application

Concrètement, dans votre code source, aucune référence au rôle courant de l'utilisateur ne doit être faite, sous peine de perdre l'aspect dynamique et personnalisable de la liste des rôles. Les privilèges seront utilisés à la place.

### Quelques mots sur BjyAuthorize...

BjyAuthorize est un module de gestion des droits qui est utilisé par UnicaenAuth. Il exploite les ACL de Zend et fait le lien avec votre application par le biais :

- de directives de configuration des droits
- d'assertions pour aller plus loin si nécessaire
- de plugins et d'aides de vues (isAllowed) qui permettent à tout moment de tester si on a le droits de quelque chose ou non.

### Directives de configuration

#### Accès à la liste des privilèges

Pour

- bénéficier de l'auto-complétion,
- pouvoir recenser les usages d'un privilège dans votre application,
- éviter des erreurs (oubli de caractères, etc.),

il est nécessaire d'avoir une constante pour chaque privilège. Ces constantes peuvent être rassemblées dans un unique fichier Application/Provider/Privilege/Privileges.php avec un contenu similaire à :

    <?php
     
    namespace Application\Provider\Privilege;
     
    /**
     * Description of Privileges
     *
     * Liste des privilèges utilisables dans votre application
     *
     * @author UnicaenCode
     */
    class Privileges extends \UnicaenAuth\Provider\Privilege\Privileges {
     
        const ALBUM_VISUALISATION           = 'album-visualisation';
        const ALBUM_EDITION                 = 'album-edition';
     
    }

*Les privilèges listés sont composés à la fois de la catégorie et du privilège.*

Ce code pourra être géréré automatiquement avec le générateur de code d'UnicaenCode (page GeneratePrivileges) et directement copié/collé dans votre projet. Lorsque vous ajouterez un privilège à votre base de données, l'opération sera à recommencer.

#### Guards

Les Guards servent à protéger les accès aux actions de contrôleurs. Concrètement, si un utilisateur n'a pas le droit d'accéder à une action, alors une erreur 403 lui est retournée.

Exemple de configuration de guards :

    'bjyauthorize'    => [
            'guards'             => [
                /* Utilisation du système de privilèges d'UnicaenAuth pour les guards */
                \UnicaenAuth\Guard\PrivilegeController::class => [
                    /* Global */
                    [
                        'controller' => 'Application\Controller\OffreFormation',
                        'action'     => ['search-structures', 'search-niveaux'],
                        'privileges' => Privileges::ODF_VISUALISATION,
                    ],
                    /* Éléments pédagogiques */
                    [
                        'controller' => 'Application\Controller\OffreFormation\ElementPedagogique',
                        'action'     => ['voir', 'search', 'getPeriode'], // getPeriode est utilisé pour la saisie de service!!!
                        'privileges' => Privileges::ODF_ELEMENT_VISUALISATION,
                    ],
                    [
                        'controller' => 'Application\Controller\OffreFormation\ElementPedagogique',
                        'action'     => ['saisir', 'supprimer'],
                        'privileges' => Privileges::ODF_ELEMENT_EDITION,
                        // usage avancé d'une assertion pour préciser les droits
                        'assertion'  => 'OffreDeFormationAssertion', 
                    ],
                    /* Rétro-compatibilité avec le système de BjyAuthorize : on peut aussi fournir des rôles... */
                    [
                        'controller' => 'Application\Controller\OffreFormation\OffreFormation',
                        'action'     => ['index'],
                        'roles'      => ['user'], // tous les utilisateurs connectés ont accès à cette page
                    ],
                ],
            ],
        ],

privileges peut accueillir aussi un tableau.

*Attention à bien faire un use pour pouvoir utiliser simplement Privileges ou à défaut à utiliser \Application\Provider\Privilege\Privileges*

*Attention : les versions stables de BjyAuthorize ne gèrent pas les assertions dans des guards. Seule la version actuellement en développement (dev-master) le gère. Attention à bien configurer votre composer, donc, si vous voulez faire des assertions sur des guards!!*

#### Resources

##### Utilisation de ressources existantes

Avec BjyAuthorize, tous les contrôleurs et toutes les actions de contrôleurs sont identifiés comme des ressources.

La ressource d'une action de contrôleur est une chaîne de caractères qui peut être récupérée de la manière suivante :

    $resourceAlbumIndex = \UnicaenAuth\Guard\PrivilegeController::getResourceId('Application\Controller\Album', 'index');

Avec UnicaenAuth, les privilèges sont aussi assimilables à des ressources. La ressource d'un privilège est une chaîne de caractères qui peut être récupérée de la manière suivante :

    use Application\Provider\Privilege\Privileges;
     
    $resourceAlbumEdition = Privileges::getResourceId(Privileges::ALBUM_EDITION);

##### Définition de vos propres ressources

Pour créer une ressource, il suffit de la déclarer dans votre configuration :

    'bjyauthorize' => [
        'resource_providers' => [
            'BjyAuthorize\Provider\Resource\Config' => [
                'ElementPedagogique' => [],
                'Etape'              => [],
            ],
        ],
    ],

Si vous voulez qu'une entité devienne une ressource, il faut qu'elle implémente ResourceInterface :

    namespace Application\Entity\Db;
     
    use Laminas\Permissions\Acl\Resource\ResourceInterface;
     
    class ElementPedagogique implements ResourceInterface
    {
     
        /**
         * Returns the string identifier of the Resource
         *
         * @return string
         */
        public function getResourceId()
        {
            return 'ElementPedagogique'; // à déclarer aussi dans votre config comme ci-dessus
        }
     
    }

Ceci est valable pour n'importe quelle classe. Il n'y a pas que les entités qui peuvent servir de ressources.

#### Rules

Les Rules sont des règles de gestion qui permettent de mettre en relation :

- un ou plusieurs privilèges ou pas
- une ou plusieurs ressources ou pas
- une assertion ou pas
- un ou plusieurs rôles ou pas (utile pour user et guest, pas recommandé pour d'autres rôles)

Exemple de configuration de Rules :

    'bjyauthorize' => [
        'rule_providers'     => [
            \UnicaenAuth\Provider\Rule\PrivilegeRuleProvider::class => [
                'allow' => [
                    [
                        'privileges' => Privileges::ODF_ELEMENT_EDITION,
                        'resources'  => ['ElementPedagogique', 'Structure'],
                        'assertion'  => 'AssertionOffreDeFormation',
                    ],
                    [
                        'privileges' => Privileges::ODF_ETAPE_EDITION,
                        'resources'  => ['Etape', 'Structure'],
                        'assertion'  => 'AssertionOffreDeFormation',
                    ],
                    [
                        'privileges' => Privileges::ODF_CENTRES_COUT_EDITION,
                        'resources'  => ['Etape', 'Structure', 'ElementPedagogique', 'CentreCoutEp'],
                        'assertion'  => 'AssertionOffreDeFormation',
                    ],
                    [
                        'privileges' => Privileges::ODF_MODULATEURS_EDITION,
                        'resources'  => ['Etape', 'Structure', 'ElementPedagogique', 'ElementModulateur'],
                        'assertion'  => 'AssertionOffreDeFormation',
                    ],
                ],
            ],
        ],    
    ],

*Attention : ici, la compatibilité avec Bjyauthorize n'est pas gérée pour le format de configuration : BjyAuthorize impose en effet un format tableau simple [roles,ressources,privileges,assertion] alors qu'avec le module UnicaenAuth une table de hachage est nécessaire (clés privileges, resources, etc) comme pour les guards.*

### Assertions

Une assertion est un service qui hérite de UnicaenAuth\Assertion\AbstractAssertion.

Elle doit être déclarée dans la configuration du ServiceManager, comme n'importe quel autre service réutilisable.

Son rôle est de répondre true ou false à une demande en fonction d'un certain nombre d'éléments qui lui sont transmis le cas échéant.

AbstractAssertion vous fournit l'accès
- au ServiceLocator,
- au gestionnaire d'événements MVC (Laminas\Mvc\MvcEvent) pour éventuellement récupérer des infos sur les paramètres d'URL ou de routeur, etc (utile pour les assertions lancées par les guards),
- à l'ACL courante (getAcl()),
- au rôle courant (getRole()).

De plus, AbstractAssertion va vous pré-macher le travail en déterminant si l'assertion concerne un privilège, une action de contrôleur, une entité ou autre chose.

Il ne reste donc plus qu'à fournir une réponse à la question en héritant les méthodes suivantes (qui renvoient juste true par défaut) :

    /**
     * @param string        $privilege
     * @param string        $subPrivilege
     *
     * @return boolean
     */
    protected function assertPrivilege($privilege, $subPrivilege = null)
 
    /**
     * @param string        $controller
     * @param string        $action
     * @param string        $privilege
     *
     * @return boolean
     */
    protected function assertController($controller, $action = null, $privilege = null)
 
    /**
     * @param ResourceInterface $entity
     * @param string            $privilege
     *
     * @return boolean
     */
    protected function assertEntity(ResourceInterface $entity, $privilege = null)
 
    /**
     * @param ResourceInterface $resource
     * @param string            $privilege
     *
     * @return boolean
     */
    protected function assertOther(ResourceInterface $resource = null, $privilege = null)
 

*Vos méthodes devront systématiquement renvoyer true ou false, rien d'autre.*

Voici maintenant un exemple concret d'assertion :

    <?php
     
    namespace Application\Assertion;
     
    use Application\Provider\Privilege\Privileges;
    use Application\Entity\Db\CentreCoutEp;
    use Application\Entity\Db\ElementModulateur;
    use Application\Entity\Db\ElementPedagogique;
    use Application\Entity\Db\Etape;
    use Application\Entity\Db\Source;
    use Application\Entity\Db\Structure;
    use UnicaenAuth\Assertion\AbstractAssertion;
    use Application\Acl\Role;
    use Laminas\Permissions\Acl\Resource\ResourceInterface;
     
     
    /**
     * Description of OffreDeFormationAssertion
     *
     * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
     */
    class OffreDeFormationAssertion extends AbstractAssertion
    {
        protected function assertEntity(ResourceInterface $entity = null, $privilege = null)
        {
            $role = $this->getRole();
            // Si le rôle n'est pas renseigné alors on s'en va...
            if (!$role instanceof Role) return false;
            // pareil si le rôle ne possède pas le privilège adéquat
            if (!$this->getAcl()->isAllowed($role, Privileges::getResourceId($privilege))) return false;
     
            // Si c'est bon alors on affine...
            switch(true){
                case $entity instanceof ElementPedagogique:
                    switch ($privilege) {
                        case Privileges::ODF_ELEMENT_EDITION:
                            return $this->assertElementPedagogiqueSaisie($role,$entity);
                        case Privileges::ODF_CENTRES_COUT_EDITION:
                            return $this->assertElementPedagogiqueSaisieCentresCouts($role, $entity);
                        case Privileges::ODF_MODULATEURS_EDITION:
                            return $this->assertElementPedagogiqueSaisieModulateurs($role, $entity);
                    }
                    break;
                case $entity instanceof Etape:
                    switch ($privilege) {
                        case Privileges::ODF_ETAPE_EDITION:
                            return $this->assertEtapeSaisie($role, $entity);
                        case Privileges::ODF_CENTRES_COUT_EDITION:
                            return $this->assertEtapeSaisieCentresCouts($role, $entity);
                        case Privileges::ODF_MODULATEURS_EDITION:
                            return $this->assertEtapeSaisieModulateurs($role, $entity);
                    }
                    break;
                case $entity instanceof Structure:
                    switch ($privilege) {
                        case Privileges::ODF_ETAPE_EDITION:
                        case Privileges::ODF_ELEMENT_EDITION:
                        case Privileges::ODF_CENTRES_COUT_EDITION:
                        case Privileges::ODF_MODULATEURS_EDITION:
                            return $this->assertStructureSaisie($role, $entity);
                    }
                    break;
                case $entity instanceof CentreCoutEp:
                    switch ($privilege) {
                        case Privileges::ODF_CENTRES_COUT_EDITION:
                            return $this->assertCentreCoutEpSaisieCentresCouts($role, $entity);
                    }
                    break;
                case $entity instanceof ElementModulateur:
                    switch ($privilege) {
                        case Privileges::ODF_MODULATEURS_EDITION:
                            return $this->assertElementModulateurSaisieModulateurs($role, $entity);
                    }
                    break;
            }
     
            return true;
        }
     
     
     
        /* ---- Edition étapes & éléments ---- */
        protected function assertElementPedagogiqueSaisie(Role $role, ElementPedagogique $elementPedagogique)
        {
            return $this->assertStructureSaisie($role, $elementPedagogique->getStructure())
            && $this->assertSourceSaisie($elementPedagogique->getSource());
        }
     
     
     
        protected function assertEtapeSaisie(Role $role, Etape $etape)
        {
            return $this->assertStructureSaisie($role, $etape->getStructure())
            && $this->assertSourceSaisie($etape->getSource());
        }
     
     
     
        /* ---- Centres de coûts ---- */
        protected function assertEtapeSaisieCentresCouts(Role $role, Etape $etape)
        {
            return $this->assertStructureSaisie($role, $etape->getStructure())
            && $etape->getElementPedagogique()->count() > 0;
        }
     
     
     
        protected function assertCentreCoutEpSaisieCentresCouts(Role $role, CentreCoutEp $centreCoutEp)
        {
            return $this->assertElementPedagogiqueSaisieCentresCouts($role, $centreCoutEp->getElementPedagogique());
        }
     
     
     
        protected function assertElementPedagogiqueSaisieCentresCouts(Role $role, ElementPedagogique $elementPedagogique)
        {
            return $this->assertStructureSaisie($role, $elementPedagogique->getStructure());
        }
     
     
     
        /* ---- Modulateurs ---- */
        protected function assertEtapeSaisieModulateurs(Role $role, Etape $etape)
        {
            return $this->assertStructureSaisie($role, $etape->getStructure())
            && $etape->getElementPedagogique()->count() > 0;
        }
     
     
     
        protected function assertElementPedagogiqueSaisieModulateurs(Role $role, ElementPedagogique $elementPedagogique)
        {
            return $this->assertStructureSaisie($role, $elementPedagogique->getStructure());
        }
     
     
     
        protected function assertElementModulateurSaisieModulateurs(Role $role, CentreCoutEp $centreCoutEp)
        {
            return $this->assertElementPedagogiqueSaisieCentresCouts($role, $centreCoutEp->getElementPedagogique());
        }
     
     
     
        /* ---- Globaux ---- */
        protected function assertStructureSaisie(Role $role, Structure $structure)
        {
            // Dans Ose (d'où vient cette assertion), les rôles sont parfois liés à une structure. Pas dans UnicaenAuth
            if ($rs = $role->getStructure()) { 
                return $rs === $structure;
            }
     
            return true;
        }
     
     
     
        protected function assertSourceSaisie(Source $source)
        {
            return $source->isOse();
        }
    }

Ici, seule assertEntity est utilisée. Les autres types d'assertions ne sont pas nécessaires. À première vue, les imbrications de switch/case d'AssertEntity peuvent paraître lourdes. Cependant cela permet de faire ruisseler les données jusqu'au test final, qui est accessible directement au besoin (si on fournit directement la structure plutôt qu'un élément pédagogique à isAllowed). AssertEntity en lui-même ne s'occupe que du routage. Cette manière d'architecturer votre assertion est recommandée car votre code pourra rester intelligible et adaptable y compris avec de nombreux tests.

## Usages avec IsAllowed

Maintenant que tout est défini, il ne reste plus qu'à faire les tests de droits dans notre code source.

IsAllowed est fait pour cela. C'est à la fois un plugin de contrôleur et une aide de vue. Donc aussi bien dans un contrôleur que dans une vue, $this->isAllowed(/* vos paramètres */); marchera.

Pour les guards, isAllowed est appelé implicitement par le GuardController. Inutile donc de répéter le test.

*Attention : les assertions peuvent ralentir votre application si vous en abusez. Je vous conseille de ne les utiliser qu'avec parcimonie ;-)*

Exemples :

    $elementPedagogique = /* Récupération d'un élément pédagogique */
    $canEditElement = $this->isAllowed($elementPedagogique,Privileges::ODF_ELEMENT_EDITION);
    if ($canEditElement) echo 'On a le droit d\'éditer cet élément pédagogique particulier';
     
     
    /* Depuis une aide de vue */
    $canCreateEtape = $this->getView()->isAllowed(Privileges::getResourceId(Privileges::ODF_ETAPE_EDITION));
    //utile pour afficher de manière conditionnelle un bouton d'ajout d'étape à partir du privilège correspondant
     
     
    /* Savoir si on a le droit d'effectuer l'action de saisir un album */
    $canGo = $this->isAllowed(\UnicaenAuth\Guard\PrivilegeController::getResourceId('Application\Controller\Album', 'saisie');
    if ($canGo) echo '<a href="'.$this->url('album/saisie').'">Saisie d\'album</a>';
   
### Et si on n'est pas dans une vue ou un contrôleur ?
    
isAllowed est avant tout une méthode publique du service Authorize de BjyAuthorize. Il suffit de récupérer ce service avec le ServiceLocator et mancer la méthode :
    
    $serviceAuthorize = $this->getServiceLocator()->get('BjyAuthorize\Service\Authorize');
    /* @var $serviceAuthorize \BjyAuthorize\Service\Authorize */
     
    $canDoThat = $serviceAuthorize->isAllowed(/* vos paramètres */);

## Autres cas

### Comment afficher un item de menu de manière conditionnelle ?

Dans la configuration, il suffit d'affecter une resource à un élément de navigation. L'accès à la ressource va conditionner la visibilité de l'élément.

    'navigation'      => [
            'default' => [
                'home' => [
                    'pages' => [
                        'of' => [
                            'label'    => 'Offre de formation',
                            'title'    => "Gestion de l'offre de formation",
                            'route'    => 'of',
                            // exemple en récupérant le ressource d'une action de contrôleur
                            'resource' => \UnicaenAuth\Guard\PrivilegeController::getResourceId('Application\Controller\OffreFormation', 'index'),
                        ],
                    ],
                ],
            ],
        ],



[roles]: ./roles.png
[privileges]: ./privileges.png
[systeme_de_gestion_des_droits]: ./systeme_de_gestion_des_droits.png

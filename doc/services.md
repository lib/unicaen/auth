# Liste des services proposés

Voici la liste des services proposées :

| Nom |	Classe | Description |
| ----|--------|-------------|
| UnicaenAuth\Service\Privilege |          UnicaenAuth\Service\PrivilegeService |	       Gère les privilèges (depuis la BDD)
| UnicaenAuth\Service\CategoriePrivilege | UnicaenAuth\Service\CategoriePrivilegeService | Gère les catégories de privilèges (depuis la BDD)
| UnicaenAuth\Service\Role |               UnicaenAuth\Service\RoleService |               Gère les rôles (depuis la BDD)
| UnicaenAuth\Service\UserContext |        UnicaenAuth\Service\UserContext |               Fournit un ensemble de méthodes liés à l'utilisateur
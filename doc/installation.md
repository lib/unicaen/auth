# Installation

Cette page traite de l'installation du module UnicaenAuth au sein d'une application ne l'utilisant par encore.

## Module

- Éditez le fichier composer.json se trouvant à la racine de votre projet et assurez-vous que le “repository” suivant est bien présent :

composer.json

        "repositories": [
            { 
                "type": "composer", 
                "url": "http://dev.unicaen.fr/packagist" 
            }
        ],

- Ajoutez à présent la dépendance suivante :

composer.json

        "require": {
            ...
            "unicaen/unicaen-auth": "dev-master"
        },
        "minimum-stability": "dev"

- Placez-vous à la racine de votre projet et lancez la commande suivante dans un shell :

$ php ../composer.phar update

La commande ci-dessus fonctionne seulement si le binaire composer.phar se trouve dans le répertoire parent. Plus d'infos : http://getcomposer.org.

- Activez les modules suivants dans cet ordre dans le fichier config/application.config.php de l'application :

    'modules' => array(
        'Application',
        'ZfcBase', 'DoctrineModule', 'DoctrineORMModule', 'ZfcUser', 'BjyAuthorize', 
        'UnicaenApp', 'AssetManager',
        'UnicaenAuth',
        // ...
    ),

## Base de données

Des tables doivent être créées/initialisées dans une base de données si vous prévoyez d'activer l'une des fonctionnalités suivantes :

- enregistrement / mise à jour de l'utilisateur authentifié dans la table des utilisateurs de l'appli (save_ldap_user_in_database) ;
- création d'un compte utilisateur par l'utilisateur lui-même (enable_registration) ;
- attribuer des rôles aux utilisateurs dans la base de données.
- gestion complète des rôles et privilèges

Voici le script à utiliser :

    vendor/unicaen/unicaen-auth/data/schema.sql

Vous pouvez paramétrer l'accès à la base de données : par défaut 'doctrine.entitymanager.orm_default' est utilisé mais vous pouvez en choisir un autre (dans la config, modifier le paramètre unicaen-auth/entity_manager_name).

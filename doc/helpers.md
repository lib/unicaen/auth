# Aides de vue (view helpers)

## AppConnection

Aide de vue générant le lien et les infos concernant la connexion à l'application.

## UserConnection

Aide de vue générant le code HTML du lien de connexion ou de déconnexion à l'appli selon que l'utilisateur est connecté ou non.

## UserCurrent

Aide de vue générant le code HTML d'affichage de toutes les infos concernant l'utilisateur courant, à savoir :

- “Aucun” + lien de connexion OU BIEN nom de l'utilisateur connecté + lien de déconnexion
- profil de l'utilisateur connecté (ex: standard, gestionnaire, administrateur, etc.)
- infos administratives sur l'utilisateur (affectation, responsabilité)

## UserInfo

Aide de vue générant le code HTML d'affichage des infos administratives sur l'utilisateur (affectations, responsabilités).

## UserProfile

Aide de vue permettant d'afficher le profil de l'utilisateur connecté.

## UserStatus

Aide de vue de rendu des éléments concernant le statut de connexion à l'appli, à savoir :

- si un utilisateur est connecté : l'identité de l'utilisateur connecté et un lien pointant vers la demande de déconnexion
- sinon : un lien pointant vers le formulaire de connexion

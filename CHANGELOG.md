CHANGELOG
=========

6.0.7
-----
- Redirection des mails vers 'CURRENT_USER' (utilisateur connecté) : en cas d'usurpation, l'utilisateur ciblé est l'usurpateur. 

6.0.6
-----
- [FIX] La redirection des mails vers 'CURRENT_USER' (utilisateur connecté) ne fonctionnait pas si aucun utilisateur n'était connecté !
- Page de confirmation de changement de mot de passe : lien vers l'accueil ou vers la connexion selon qu'un utilisateur est connecté ou non. 

6.0.5
-----
- [FIX] HistoriqueListener : précédemment, on injectait le AuthenticationService mais cela créait une boucle infinie de dépendances de services. 
  Donc on a choisi d'injecter le service manager :-( et c'est HistoriqueListener qui récupère lui-même le AuthenticationService au moment opportun.

6.0.4
-----
- Retour de la possibilité d'utiliser 'CURRENT_USER' dans les adresses de redirection pour rediriger les mails vers l'utilisateur connecté.
- L'entity manager est désormais injecté dans PrivilegeService par la factory (fait à l'occasion d'une traque de boucle infinie dans les services).
- Adapteur d'auth Shib : retrait d'une injection de dépendance pouvant créer une boucle infinie de dépendances de services et qui n'est plus nécessaire
  (l'IDP semble désormais rediriger correctement après la déconnexion).

6.0.3
-----
- Aide de vue UserCurrent : suppression du scrolling intempestif en haut de page lorsqu'on clique sur le nom de l'utilisateur.

6.0.2
-----
- Modif de la collecte des rôles de l'utilisateur, permettant à l'appli de substituer son rôle 'Authentifié' maison à celui par défaut.
- Identity provider 'Db' : inutile de tester les 'ldap_filter' de chaque rôle si aucun LDAP activé.

6.0.1
-----
- [FIX] Utilisation à tort de l'option de config 'shib_user_id_extractor' sur des données d'usurpation/simulation.

6.0.0
-----
- PHP 8 requis.

5.1.1
-----
- [FIX] Utilisation à tort de l'option de config 'shib_user_id_extractor' sur des données d'usurpation/simulation.  

5.1.0 = 5.0.0
-----
- Migration vers Bootstrap 5 (front-end).
- Infos à propos de la connexion dans le menu principal : simple bouton 'Connexion' si aucun utilisateur connecté ;
  bouton 'Déconnexion' déplacé dans le popover.

4.0.2
-----
- RoleFormatter : correction pour exploiter la méthode __toString() si présente.

4.0.1
-----
- Possibilité d'activer ou non (en config) les logs des échecs d'authentification LDAP.

4.0.0
-----
- Passage de Zend à Laminas


3.2.10
-----
- Possibilité d'activer ou non (en config) les logs des échecs d'authentification LDAP.

3.2.8
-----
- [FIX] Données d'authentification : utilisation du SessionManager global pour avoir les durées de conservation des cookies correctes.

3.2.6 / 3.2.7
-----
- Ajout d'un événement avec le détail des erreurs LDAP

3.2.5
-----
- [FIX] Petite correction d'une NOTICE qui apparaissait lorsqu'on se déconnectait.

3.2.3
-----
- [FIX] Meilleure gestion d'erreur en cas de demande d'usurpation d'un compte inexistant : retour à l'accueil + message.

3.2.2
-----
- L'URL de connexion accepte désormais role=ROLE_ID pour sélectionner automatiquement ce rôle une fois authentifié.

3.2.1
-----
- Modifications/améliorations pour faciliter le support d'autres modes d'authentification (ex: unicaen/auth-token). 
- Le type d'authentification souhaité (local, shib ou cas) peut être spécifié dans l'URL de redirection via le 
  query param 'authtype'
- Ajout de la colonne CREATED_AT dans les scripts SQL de création de la table USER (non mappée dans l'entité).  
- [FIX] Usurpation d'un compte local (db) depuis une authentification shib
- [FIX] Une chaîne vide doit être considérée comme null dans ShibService::extractShibUserIdValueForDomainFromShibData()
- [FIX] Nécessité de clés littérales dans la config par domaine de 'shib_user_id_extractor' sinon doublons lors de la 
  fusion des configs


3.2.0
-----
- Configuration de la stratégie d'extraction d'un identifiant utile parmi les données d'authentification shibboleth
  (config 'shib_user_id_extractor').
- Possibilité de connaître la source de l'authentification (db, ldap, cas, shib).
- Possibilité de stopper l'usurpation en cours pour revenir à l'identité d'origine.
- Enregistrement du dernier rôle sélectionné pour pouvoir y revenir à la reconnexion.
  *ATTENTION : nouvelle colonne `LAST_ROLE_ID` à créer dans la table `USER`, cf. SQL dans [data/](./data/).*
- [FIX] Usurpation d'un compte local en BDD.
- [FIX] Lorsque l'usurpateur stoppait l'usurpation, il ne récupérait pas son dernier endossé
- [FIX] En cas d'usurpation puis de déconnexion puis de reconnexion, on revenait dans la peau de l'usurpé sans pouvoir
  stopper l'usurpation


3.1.2
-----
- Aide de vue UserUsurpationHelper : ajout de la possibilité de dessiner un simple bouton.


3.1.1
-----
- Correction bug : test d'activation manquant dans les adapteurs Db et Ldap.


3.1.0
-----
- Typage des authentifications
  - Pages de connexion différentes selon le type d'authentification : shib ; db ou ldap ; cas.
  - Possibilité d'ordonner les formulaires de connexion proposés (config).
  - Possibilité d'ajouter une description HTML à chaque formulaire de connexion (config).
  - Chaque adapter peut désormais tester s'il est compétent pour traiter la requête d'authentification.
  - Création d'un adapter d'authentification comme les autres pour Shib.
- Réparation du mécanisme de redirection vers l'URL demandée avant connexion.
- Correction du bug de rémanence de l'authentification shibboleth simulée.
- Authentification LDAP : levée d'une exception en cas de survenue d'une erreur LDAP.


3.0.12 (05/11/2020)
-------------------
- Ajout d'une méthode pour pouvoir purger la liste des rôles courante.


3.0.11 (07/10/2020)
-------------------
- Affichage de l'utilisateur connecté : ajout d'un triangle indiquant que ça se déroule.


3.0.1 (18/09/2019)
------------------
- Corrections
  - Correction de ModuleOptionsFactory qui ne respectait son interface FactoryInterface.


3.0.0 (17/09/2019)
------------------
Première version officielle sous ZF3.
